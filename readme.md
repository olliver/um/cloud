Hidden Print System API
======================================================
**This document is more readable on [DocumentUp](http://documentup.com/Doodle3D/Cloud)** 

The Hidden Print System makes 3D printers wirelessly controllable through an API using [Socket.io](http://socket.io/) and REST. This means you can control your 3D prints using JavaScript. In the future more languages can be made compatible with this API.

So how to use the Hidden Print System? You can think of the UltimakerApp as the first application written on top of the API. Another simple example is our localprinters overview page, at [doodle3d.com/localprinters](http://doodle3d.com/localprinters), it should display all the printers on your network. <br>
This API makes it possible to control a 3D printer with data from a Kinect, make prints based on sound input, create a cup design app or add an embeddable print button to an existing design application. 

The API currently lives at: https://cloud.doodle3d.com/

Register
--------------------------------------
To access the Socket.io API you need to create an account. This is currently the only REST API part of the system.

### POST: /user/register
#### Response
- **key**: (string) The user's login. Used as url parameter to access the Socket.io namespaces (this is private).
- **printers**: (array) A list of user's preferred printers (which should still be empty).
- **id**: (string) The user's id.

#### Content example
``` json
{
  "key": "546b4e108615e2051a6d7623E7gkT9UApGpJvPm7Q6IU",
  "printers": [],
  "id": "106b4e598615e2051a6d7622"
}
```

#### Example
You can retrieve a new key using for example the jQuery ajax method. 
``` javascript
$.post( 'https://cloud.doodle3d.com/user/register', function(response) {
  console.log("key: ",response.key);
},'json');
```

Socket.io basics
--------------------------------------
The basic idea of socket.io is that you can connect to different *namespaces* and inside each you can send and receive *events*. 

You can get the javascript Socket.io client from: <br>
https://cloud.doodle3d.com/socket.io/socket.io.js <br>
That file will stay in sync with the socket.io version of this platform; but you can also include it locally using bower or npm.

#### Documentation Abbreviations
To summarise the characteristics of events throughout the documentation of the API we use some abbreviations:
- **>** : Can be sent by user to printer/server (execute).
- **<** : Is sent by printer to users, on changes.
- **><** : Can be requested by users to printers/server.
- **<+** : Same as **<**, but is also sent when you connect to a namespace.

### Examples

#### Basic print example
Make all the local printers print robots. 
``` javascript
var url = 'https://cloud.doodle3d.com/';
var key = '?key={REPLACE WITH KEY}';
var robotSTL = 'https://www.doodle3d.com/stl/robot.stl';

// connect to /localprinters namespace with key
var localPrintersSocket = io(url+'localprinters'+key);
// when a local printer appears
localPrintersSocket.on("appeared",function(printerData) { 
  console.log('printer appeared: ',printerData);
  // connect to printer's printer feature namespace
  var printingSocket = io(url+printerData.id+'-printer'+key);
  printingSocket.once('connection',function() { 
    // print the Ultimaker robot 
    printingSocket.emit('print',{url:robotSTL});
    // listen to the printer state 
    printingSocket.on('state',function(data) {
      console.log('printer state: ',data.state);
    });
  });
});
localPrintersSocket.once('error',function(err) {
  console.log(nsp+": error: ",err);
});
```

#### Preventing multiple listeners
When you create an application that regularly disconnects and reconnects sockets (to minimize internet traffic) you can easily create multiple listeners by accident. There are a few things to keep in mind to prevent this from happening. <br>
When you listen to the *connection* event before you start listening or sending it's important to use `once` instead of `on`, because the *connection* event is also emitted when the user automatically reconnects. This can happen when the internet connection is unreliable. Using `once` makes sure you only do something the first time the event is received from the socket. 
The `io` method returns a reference to the new or existing socket connection. You can use this to re-use the same socket in multiple places. This also means that you need to make sure not to add the same listeners multiple times. A trick to do this is to check the socket.connected property, this is set to false the first time because it's not connected yet. 

``` javascript
var url = 'https://cloud.doodle3d.com/';
var key = '?key={REPLACE WITH KEY}';
var robotSTL = 'https://www.doodle3d.com/stl/robot.stl';

// connect to /localprinters namespace with my key 
// returns new or existing socket connection
var localPrintersSocket = io(url+'localprinters'+key);
// make sure we only attach listeners only once (when the socket isn't connected yet)
if(!localPrintersSocket.connected) { 
  // when we are connected for the first time (once)
  localPrintersSocket.once('connection',function() { 
    console.log('connected to namespace');
    // when a local printer appears
    localPrintersSocket.on("appeared",function(printerData) { 
      console.log('printer appeared: ',printerData);
      // connect to printer's printer feature namespace
      var printingSocket = io(url+printerData.id+'-printer'+key);
      if(!printingSocket.connected) { 
        printingSocket.once('connection',function() { 
          // print the Ultimaker robot 
          printingSocket.emit('print',{url:robotSTL});
          // listen to the printer state 
          printingSocket.on('state',function(data) {
            console.log('printer state: ',data.state);
          });
        });
      }
    });
  });
  localPrintersSocket.once('error',function(err) {
    console.log(nsp+": error: ",err);
  });
});
```

### Finding printers
You will usually start by connecting to the [/localprinters](#localprinters) namespace to retrieve the printers that are on the users local network. Per printer you will receive an *id*, a *name* and a list of *features*. 
When you already have preferred printers, they can retrieved through the [/printers](#printers) namespace.

### Printer communication
#### Namespaces
When you have acquired the printer id (*prid*) you, can connect to the printer-specific namespaces. These always start with the the *id* of the printer. So the main namespace ([/{prid}](#prid)) of a printer with the *id*: *106b4e598615e2051a6d7622* is: `/106b4e598615e2051a6d7622`. There you can find basic information such as the printer's state and wifi state. <br>
For control and more extensive information each printer has feature namespaces. The syntax is: `/{prid}-{feature}`. So a printer with *id* *106b4e598615e2051a6d7622* and *features* such as *printer*, *slicer*, *network*, *webcam*, *debug* and *update* will create the following namespaces:
- `/106b4e598615e2051a6d7622`
- `/106b4e598615e2051a6d7622-printer`
- `/106b4e598615e2051a6d7622-slicer`
- `/106b4e598615e2051a6d7622-network`
- `/106b4e598615e2051a6d7622-webcam`
- `/106b4e598615e2051a6d7622-debug`
- `/106b4e598615e2051a6d7622-update`

Not all printers will support all features, so it's smart to check the `features` that are supported by the printer before connecting to their corresponding namespaces.

#### Events
Inside namespaces you can send and receive events. See the [Examples](#Examples). 

### Rules
- Socket.io is used as it was originally designed.
- Data is always communicated in [JSON format](http://json.org/).
- To minimize internet traffic application, events are spread over multiple namespaces. This also keeps event names simple. Socket.io will also try to multiplex multiple socket connections over one websocket connection.
- Where appropriate, event names start with the request type: get/set/add/remove/update/reset. 
- All events from the server contain key/value pairs, never a value or array directly. This keeps the content extensible.
- All non-"expensive" information is automatically sent when a user connects to a namespace (<+), more "expensive" information must be requested manually (><).
- Executes (>) and Requests (><) can both return acknowledgement data through a callback function (using socket.io's acknowledgment callbacks). Requests can also return additional data.
- Callback function responses will receive a possible error as first argument. This error object should contain:
  - name: (string) Machine readable name, like ValidationError, InvalidArgumentsError, etc. 
  - message: (string) Optional more explanatory message. 
- When there are multiple errors there will be an issues array in the error object. Each item should contain:
  - name: (string) Machine readable name, like OutOfRange.
  - message: (string) Optional more explanatory message. 
  - subject: (string) Optional subject of specific error, like the name of a specific field that didn't validate.  
- Callback function responses will receive `null` as first argument when there are no errors.
- Callback function responses can receive a possible data object as second argument. This could contain requested data, like found networks.
- Event data can also contain an err property when there are errors. This err property follows the callback error rules. 
- States are always reported as strings. 
- Percentages range from 0 to 1. 
- An online event is emitted in all the printer specific namespaces, see: [/{prid} > online](#prid/online).

/localprinters
--------------------------------------
Used to retrieve the printers that are on the local network (behind the same remote IP) of the user. 
### list <+
A complete list of printers that are on the user's local network. Received when the user connects to namespace and when printers appear on or disappear from the user's local network.

#### Contains
- **printers** (array)
  - per printer (object):
    - **id**: (string) id of printer
    - **name**: (string) name of printer (factory name)
    - **features**: (array) supported feature names
    - **online**: (boolean) whether the printer is connected to the cloud (should be true)
    
#### Content example
``` json
{
  "printers": [
    {
      "id": "106b4e598615e2051a6d7622", 
      "name": "Ultimaker-nd91kf", 
      "features": ["printer","slicer","network","webcam","debug","update"], 
      "online": true
    },
    {
      "id": "106b4e598615e2051a6d7623", 
      "name": "Ultimaker-nd91ke", 
      "features": ["printer","slicer","network","webcam","debug","update"], 
      "online": true
    }
  ]
}
```

### appeared <+ 
Received when user connects to namespace for each already connected local printer and when a printer appears on users local network.

#### Contains
- **id**: (string) id of printer
- **name**: (string) name of printer (factory name)
- **features**: (array) supported feature names
- **online**: (boolean) whether the printer is connected to the cloud (should be true)
    
#### Content example
``` json
{
  "id": "106b4e598615e2051a6d7622", 
  "name": "Ultimaker-nd91kf", 
  "features": ["printer","slicer","network","webcam","debug","update"], 
  "online": true
}
```

### disappeared <
Received when a printer disappears from users local network.

#### Contains
- **id**: (string) id of printer
- **name**: (string) name of printer (factory name)
- **features**: (array) supported feature names
- **online**: (boolean) whether the printer is connected to the cloud (should be false)
    
#### Content example
``` json
{
  "id": "106b4e598615e2051a6d7622", 
  "name": "Ultimaker-nd91kf", 
  "features": ["printer","slicer","network","webcam","debug","update"], 
  "online": true
}
```

/printers
--------------------------------------
### list <+ 
Complete list of users preferred printers. Received when user connects to namespace and when printers are added to or removed from users preffered printers list. 

#### Contains
- **printers** (array)
  - per printer (object):
    - **id**: (string) id of printer
    - **name**: (string) name of printer (factory name)
    - **features**: (array) supported feature names
    - **online**: (boolean) whether the printer is connected to the cloud
    
#### Content example
``` json
{
  "printers": [
    {
      "id": "106b4e598615e2051a6d7622", 
      "name": "Ultimaker-nd91kf", 
      "features": ["printer","slicer","network","webcam","debug","update"], 
      "online": true
    },
    {
      "id": "106b4e598615e2051a6d7623", 
      "name": "Ultimaker-nd91ke", 
      "features": ["printer","slicer","network","webcam","debug","update"], 
      "online": true
    }
  ]
}
```

### appeared <+ 
Received when user connects to namespace for each already preferred printer and when a new printer is added to the users preferred printers list.

#### Contains
- **id**: (string) id of printer
- **name**: (string) name of printer (factory name)
- **features**: (array) supported feature names
- **online**: (boolean) whether the printer is connected to the cloud
    
#### Content example
``` json
{
  "id": "106b4e598615e2051a6d7622", 
  "name": "Ultimaker-nd91kf", 
  "features": ["printer","slicer","network","webcam","debug","update"], 
  "online": true
}
```

### changed < 
Received when a property of a preferred printer changed. This is limited to the properties are also received with the *list*, *appeared* and *disapeared* events. 

#### Contains
- **id**: (string) id of printer
- **name**: (string) name of printer (factory name)
- **features**: (array) supported feature names
- **online**: (boolean) whether the printer is connected to the cloud

#### Content example
``` json
{
  "id": "106b4e598615e2051a6d7622", 
  "name": "Ultimaker-nd91kf", 
  "features": ["printer","slicer","network","webcam","debug","update"], 
  "online": true
}
```

### disappeared < 
Received when a preferred printer is removed from users preferred printers list.

#### Contains
- **id**: (string) id of printer
- **name**: (string) name of printer (factory name)
- **features**: (array) supported feature names
- **online**: (boolean) whether the printer is connected to the cloud
    
#### Content example
``` json
{
  "id": "106b4e598615e2051a6d7622", 
  "name": "Ultimaker-nd91kf", 
  "features": ["printer","slicer","network","webcam","debug","update"], 
  "online": true
}
```
### add > 
Add a printer to the preferred printers list of the user. 

#### Parameters
- **id**: (string) printer id

#### Example
Add a specific printer.
``` javascript
var url = 'https://cloud.doodle3d.com/';
var key = '?key={REPLACE WITH KEY}';
var printerID = '{REPLACE WITH PRINTER ID}';

// connect to /printers namespace with key
var printersSocket = io(url+'printers'+key);
printersSocket.once('connection',function() { 
  // add printer
  printersSocket.emit('add',{id:printerID});
});
```

### remove > 
Remove a printer from the preferred list of the user.

#### Parameters
- **id**: (string) printer id

#### Example
Remove a specific printer.
``` javascript
var url = 'https://cloud.doodle3d.com/';
var key = '?key={REPLACE WITH KEY}';
var printerID = '{REPLACE WITH PRINTER ID}';

// connect to /printers namespace with key
var printersSocket = io(url+'printers'+key);
printersSocket.once('connection',function() { 
  // remove printer
  printersSocket.emit('remove',{id:printerID});
});
```

/config
--------------------------------------
### getSlicers ><
Requests the slicer information, with per slicer contains the contentTypes and profiles. Currently the *curaEngine* is supported. In the future it could be a handy feature to implement a possibility to skip the slicer, if once sends a correctly build gcode file directly. A possible implementation for this could be a *none* slicer type.

##### Contains
- **{engine name}**: (object) Name of slicer engine. Currently only the *curaEngine* is implemented.
  - **contentTypes**: (array of strings) List of file types that are supported by this slicer. This is internally used to match the given contentType to the slicer. Such as *stl* or *obj*. Currently only *stl* files are supported.
  - **printerType**: (object with strings) Object with machine readable names as keys and human readable names as value. Printer type profiles override the default config with settings like heated bed support, start/end code tweaks etc. 
  - **material**: (object with strings) Object with machine readable names as keys and human readable names as value. Material profiles override the default config with settings like print temperatures, adhesion type etc. 
  - **profile**: (object with strings) Object with machine readable names as keys and human readable names as value. Profiles are an extra level of arbitrary overriding settings. regularly used to tweak the quality vs. speed. But you can imagine there also profiles coming for tall thin objects, for vases etc. 
  - **...**: In the future more profile types could be added. 
  
#### Content example
``` json
{
  "curaEngine": {
    "contentTypes": [
      "stl"
    ],
    "printerType": {
      "ultimaker": "Ultimaker Original",
      "ultimaker2": "Ultimaker 2"
    },
    "material": {
      "pla": "PLA",
      "abs": "ABS"
    },
    "profile": {
      "highQuality": "High quality",
      "normalQuality": "Normal quality",
      "lowQuality": "Fast low quality"
    }
  }
}
```

/{prid}
--------------------------------------
### printerState <+
Please see: [/{prid}-printer > state](#prid-printer/state).
### sliceState <+
Please see: [/{prid}-slicer > state](#prid-slicer/state).
### wifiState <+
Please see: [/{prid}-network > wifiState](#prid-network/wifistate).
### wifiSSID <+
Please see: [/{prid}-network > wifiSSID](#prid-network/wifissid).
### wifiAddress <+
Please see: [/{prid}-network > wifiAddress](#prid-network/wifiaddress).
### hotspotState <+
Please see: [/{prid}-network > hotspotState](#prid-network/hotspotstate).
### hotspotSSID <+
Please see: [/{prid}-network > hotspotSSID](#prid-network/hotspotssid).
### ethernetState <+
Please see: [/{prid}-network > ethernetState](#prid-network/ethernetstate).
### ethernetAddress <+
Please see: [/{prid}-network > ethernetAddress](#prid-network/ethernetaddress).
### networkState <+
Please see: [/{prid}-network > networkState](#prid-network/networkstate).
### networkAddress <+
Please see: [/{prid}-network > networkAddress](#prid-network/networkaddress).
### online <+
Whether this printer is online. Received when user connects to namespace or when it changes.

#### Contains
- **online**: (boolean) Whether this printer is online

#### Content example
``` json
{
  "online": true
}
```

### name <+
The name of the printer. Received when user connects to namespace or when it changes.

#### Contains
- **name**: (string) Name of printer

#### Content example
``` json
{
  "name": true
}
```

### getFeatures ><
Retrieve the features of this printer. 

#### Contains
- **features**: (array) Supported feature names

#### Content example
``` json
{
  "features": ["printer","slicer","network","webcam","debug","update"]
}
```

/{prid}-printer
--------------------------------------
### print >
Make the printer print a sliced 3d file from an online or streamed source. You can specify an url to an online file or give it a [Socket.io-stream](https://github.com/nkzawa/socket.io-stream) to send it a local file. You can send profiles to tweak the slicer. The supported contentTypes and profiles can be retrieved using [/config > getSlicers](#config/getSlicers).

#### Parameters
- **stream**: Reference to a [Socket.io-stream](https://github.com/nkzawa/socket.io-stream). Send to emit as first argument, it's not a part of the data object. See *Stream example*.
- **url**: (string) URL to a online printable file. Currently only `stl` files are supported. 
- **(filename)**: (string) (optional) 
- **(contentType)**: (string) (optional) Type of content that is sent. Currently limited to *stl*. Might be expended with types like *gcode*, *obj* etc. Default: *stl*.
- **(printerType)**: (string) (optional) Type of printer. In the future we hope to default to the printerType the printer tells us it is when it registers itself. Default: *ultimaker2*.
- **(material)**: (string) (optional) 
- **(profile)**: (string) (optional) 
- **...**: In the future the parameters could be extended with other profile types in, see [/config > getSlicers](#config/getSlicers). 
- **(settings)**: (objects) (optional) You can override the slicer engine settings by sepecifing them in this object. Check the curaEngine documentation or [code](https://github.com/Ultimaker/CuraEngine/blob/master/src/settings.h) for supported settings. 

#### URL example
``` javascript 
var url = 'https://cloud.doodle3d.com/';
var key = '?key={REPLACE WITH KEY}';
var printerID = "{REPLACE WITH PRINTER ID}";
var robotSTL = 'https://www.doodle3d.com/stl/robot.stl';

// connect to printer's printer feature namespace
var printingSocket = io(url+printerID+'-printer'+key);
printingSocket.once('connection',function() { 
  // print the Ultimaker robot 
  printingSocket.emit('print',{url:robotSTL});
});
```

#### Stream example
The following is a Node.js example.
``` javascript 
var io = require('socket.io-client');
var ss = require("socket.io-stream");
var fs = require('fs');
var url = 'https://cloud.doodle3d.com/';
var key = '?key={REPLACE WITH KEY}';
var printerID = "{REPLACE WITH PRINTER ID}";

// connect to printer's printer feature namespace
var printingSocket = io(url+printerID+'-printer'+key);
printingSocket.once('connection',function() { 
  // print a local file from the application folder files/
  var stream = ss.createStream();
  ss(printingSocket).emit('print', stream, {filename:"bunny.stl"});
  fs.createReadStream("files/bunny.stl").pipe(stream);
});
```

#### Specifing config
``` javascript 
var url = 'https://cloud.doodle3d.com/';
var key = '?key={REPLACE WITH KEY}';
var printerID = "{REPLACE WITH PRINTER ID}";
var robotSTL = 'https://www.doodle3d.com/stl/robot.stl';

// connect to printer's printer feature namespace
var printingSocket = io(url+printerID+'-printer'+key);
printingSocket.once('connection',function() { 
  // print the Ultimaker robot 
  printingSocket.emit('print',{ url:robotSTL,
                                printerType:"ultimaker",
                                material:"abs",
                                profile:"highQuality"})
});
```

#### Specifing settings
``` javascript 
var url = 'https://cloud.doodle3d.com/';
var key = '?key={REPLACE WITH KEY}';
var printerID = "{REPLACE WITH PRINTER ID}";
var robotSTL = 'https://www.doodle3d.com/stl/robot.stl';

// connect to printer's printer feature namespace
var printingSocket = io(url+printerID+'-printer'+key);
printingSocket.once('connection',function() { 
  // print the Ultimaker robot 
  printingSocket.emit('print',{ url:robotSTL,
                                profile:"highQuality"
                                settings:{layerThickness:300}});
});
```
                                    
### stop >
Stop / abort the current print.
(stream,){(url)}

<!---
### setTemperatures >
**To be implemented**<br>
Change temperatures. 
-->

#### Parameters
- **nozzle**: target nozzle temperature
- **bed**: target bed temperature

### temperatures <+
Printer temperatures update. Received when user connects to namespace or when it changes.

#### Contains
- **nozzle**: (object)
  - **current**: (number) Current temperature.
  - **target**: (number) Target temperature.
  - **percentage**: (number) Nozzle heating percentage aclopliced. Goes from 0 to 1. 
- **bed**: (object)
  - **current**: (number) Current temperature.
  - **target**: (number) Target temperature.
  - **percentage**: (number) Bed heating percentage accomplished. Goes from 0 to 1. 
- **percentage**: (number) Overall heating percentage accomplished. Goes from 0 to 1. 
- **heating**: (boolean) Wheter the printer is heating. 

#### Content example
``` json
{ 
  "nozzle": { "current": 216, "target": 220, "percentage": 0.9818181818181818 },
  "nozzle2": undefined,
  "bed": { "current": 37, "target": 70, "percentage": 0.5285714285714286 },
  "percentage": 0.7551948051948052,
  "heating": "true" 
}
```

### progress <+
Printing progress update. This is expressed in lines of the gcode it's sending to the printer. Received when user connects to namespace or when it changes.

#### Contains
- **currentLine**: (number) current gcode line it's sending to the printer.
- **totalLines**: (number) total number of gcode lines. 
- **bufferedLines**: (number) number of buffered gcode lines. 

#### Content example
``` json
{ 
  "current": 1159,
  "total": 228458,
  "buffered": 227300,
  "percentage": 0.005073142546988943 
}

```

### state <+
Printer state update. Received when user connects to namespace or when it changes.

#### Contains
- **state**: (string) State of printer. Options: 
  - *connecting*: Connecting to the electronics that control the printer.
  - *disconnected*: The electronics that control the printer is disconnected. 
  - *idle*: The printer is idle.
  - *printing*: The printer is printing. Heating is a part of printing. 
  - *stopping*: The printing is aborting a print and executing some final commands, like moving the printer to home.
  - *unknown*: The printer state is unkown.

#### Content example
``` json
{
  "state": "printing"
}
```

/{prid}-slicer
--------------------------------------
### progress <+
Progress update. Received when user connects to namespace or when it changes.

#### Contains
- **percentage**: (number 0.0 till 1.0)
- **part**: (number) Current line
- **total**: (number) Total lines
- **phase**: (string) Options as derived from the cureEngine output:
  - *inset*
  - *skin*
  - *export*
  - *process*
- **phasePart**: (number) Current line within scope of phase
- **phaseTotal**: (number) Total lines within scope of phase

#### Content example
``` json
{
  "percentage": 0.6001984126984127,
  "part": 605,
  "total": 1008,
  "phase": "skin",
  "phasePart": 269,
  "phaseTotal": 336
}
```

### state <+
Slicer state update. (Todo: to be implemented)

#### Contains
- **state**: (string) State of printer. Options: 
  - *idle*: The slicer is idle.
  - *slicing*: The slicer is slicing. 

#### Content example
``` json
{
  "state": "slicing"
}
```
<!---
### getInfo ><
ToDo: is this really implemented? 
-->

/{prid}-network
--------------------------------------
### openHotspot >
Open / create a WiFi hotspot. Because the printer can't be connected to a WiFi network and be a WiFi hotspot at the same time it might disconnect from the WiFi network it was connected to. This call makes the following steps: close hotspot if currently tethering, update found networks list, enable tethering hotspot. The network feature is based on [Connman](https://kernel.googlesource.com/pub/scm/network/connman/connman/+/1.14/doc/).

### closeHotspot >
Close the WiFi hotspot by disable tethering. 

### getNetworks ><
Get a list of available WiFi networks. When the printer is a hotspot it can't scan for networks, so it will return the networks it found before it was tethering. The `age` object shows wether the list is from a recent wifi scan or not (value `old` or `new`). Hidden networks are listed as `*hidden*`.

#### Contains
- **networks**: (array) list of networks
  - per network: (object)
    - **ssid**: (string) Name of WiFi network
    - **strength**: (number) Strength of WiFi network
    - **security**: (string) Options:
      - **none**
      - **wep**
      - **psk**
      - **ieee8021x**
      - **wps**
    - **favorite**: (bool)
    - **immutable**: (bool)
    - **autoConnect**: (bool)
  - **age** (object). Options:
    - **old**: The network list returned was captured earlier, before became tethering hotspot.
    - **new**: The network list returned is from a fresh wifi scan.

#### Content example
``` json
{ "networks":
   [ { "ssid": "Vechtclub XL F1.19",
       "state": "ready",
       "strength": 88,
       "security": ["psk"],
       "favorite": true,
       "immutable": true,
       "autoConnect": true },
     { "ssid": "Free WiFi Utrecht",
       "state": "idle",
       "strength": 61,
       "security": ["none"],
       "favorite": false,
       "immutable": false,
       "autoConnect": false },
     { "ssid": "*hidden*",
       "state": "idle",
       "strength": 60,
       "security": ["wep"],
       "favorite": false,
       "immutable": false,
       "autoConnect": false },
     { "ssid": "Today Designers",
       "state": "idle",
       "strength": 30,
       "security": ["wps"],
       "favorite": false,
       "immutable": false,
       "autoConnect": false } ],
  "age": "new" 
}
```
    
### joinNetwork >
Join a WiFi network.

#### Parameters: 
- **ssid**: (string) Name of WiFi network to connect to. 
- **(passphrase)**: (string) Password of the WiFi network, left empty for open network.

### leaveNetwork >
Leave network, but do not forget it. 

#### Parameters: 
- **ssid**: (string) Name of WiFi network.

### forgetNetwork >
Forget a network and leave it. 

#### Parameters: 
- **ssid**: (string) Name of WiFi network.

### wifiState <+
WiFi connection state update. Received when user connects to namespace or when it changes.

#### Contains
- **state**: (string) Options: 
  - *idle*: This service is not in use at all at the moment. It also is not attempting to connect or do anything else.
  - *failure*: Indicates a wrong behavior. It is similar to the "idle" state since the service is not connected.
  - *association*: This service tries to establish a low-level connection to the network. For example associating/connecting with a WiFi access point.
  - *configuration*: It is trying to retrieve/configure IP settings.
  - *ready*: Successful connected device. This doesn't mean it has the default route, but basic IP operations will succeed.
  - *online*: Internet connection is available and has been verified.
  - *disconnect*: it is going to terminate the current connection and will return to the "idle" state.
  - *unknown*

#### Content example
``` json
{
  "state": "connecting"
}
```

### wifiSSID <+
Name of the WiFi network that the printer is connected to. Received when user connects to namespace or when it changes.

#### Contains
- **ssid**: (string) 

### wifiAddress <+
Ip address of the printer on the network it's connected to over WiFi.

#### Contains
- **address**: (string) 

### hotspotState <+
WiFi hotspot connection state update. Received when user connects to namespace or when it changes.

#### Contains
- **state**: (string) Options: 
  - *unknown*
  - *creatingFailed*
  - *disabled*
  - *creating*
  - *enabled*
  - *creatingFailed*: Creating hotspot failed somehow. Check the *stateMessage* for more information. 

### hotspotSSID <+
Name of the WiFi hotspot that the printer has opened. Received when user connects to namespace or when it changes.

#### Contains
- **ssid**: (string) 

### ethernetState <+
Ethernet connection state update. Received when user connects to namespace or when it changes.

#### Contains
- **state**: (string) Options:
  - *idle*: This service is not in use at all at the moment. It also is not attempting to connect or do anything else.
  - *failure*: Indicates a wrong behavior. It is similar to the "idle" state since the service is not connected.
  - *association*: This service tries to establish a low-level connection to the network. For example associating/connecting with a WiFi access point.
  - *configuration*: It is trying to retrieve/configure IP settings.
  - *ready*: Successful connected device. This doesn't mean it has the default route, but basic IP operations will succeed.
  - *online*: Internet connection is available and has been verified.
  - *disconnect*: it is going to terminate the current connection and will return to the "idle" state.
  - *unknown*

### ethernetAddress <+
Ip address of the printer on the network it's connected to over Ethernet.

#### Contains
- **address**: (string) 

### networkState <+
Overall network connection state update. Received when user connects to namespace or when it changes.

#### Contains
- **state**: (string) Options:
  - *offline*: The printer is in offline mode
  - *idle*: This service is not in use at all at the moment. It also is not attempting to connect or do anything else.
  - *ready*: Successful connected device. This doesn't mean it has the default route, but basic IP operations will succeed.
  - *online*: Internet connection is available and has been verified.

### networkAddress <+
Ip address of the printer on the network it's connected to over it's current connection (WiFi or Ethernet).

#### Contains
- **address**: (string) 

/{prid}-webcam
--------------------------------------
### image <+
Latest webcam image. Received when user connects to namespace or when a new image is available. A snapshot is grabbed from the webcam through the MJPGSTREAMER service from the following url: 'http://localhost:8080/?action=snapshot'.

#### Example
Depends on Socket.io-stream, available at: <br>
https://github.com/nkzawa/socket.io-stream/blob/master/socket.io-stream.js <br>
Also available in npm or bower. 

``` javascript
var url = 'https://cloud.doodle3d.com/';
var key = '?key={REPLACE WITH KEY}';
var printerID = "{REPLACE WITH PRINTER ID}";

// connect to printer's webcam feature namespace
var webcamSocket = io(url+printerID+'-webcam'+key);
ss(webcamSocket).on('image', function(stream) {
  var binaryString = "";
  stream.on('data', function(data) {
    for(var i=0;i<data.length;i++) {
      binaryString+=String.fromCharCode(data[i]);
    }
  });
  stream.on('end', function() {
    imageSrc = "data:image/jpg;base64,"+window.btoa(binaryString);
    binaryString = "";
    // fill a img elements src attribute with imageSrc. 
  });
});
```

### state <+
Webcam state update. Received when user connects to namespace or when it changes.

#### Contains
- **state**: (string) Options: 
  - *enabled*
  - *disabled*
  - *unknown*

<!---
### ~~setConfig ><~~
**Not yet implemented**<br>
Condigure the MJPG streamer settings and framerate

### ~~getConfig ><~~
**Not yet implemented**<br>
Condigure the MJPG streamer settings and framerate
-->

/{prid}-debug
--------------------------------------
### getLogFiles ><
Download the logfiles

<!---
~~/{prid}-update~~
--------------------------------------
**Not yet implemented**<br>

### ~~info <+~~
**Not yet implemented**<br>
Version info update. Received when user connects to namespace or when it changes.

#### Contains
- **currentVersion**: (string)
- **currentReleaseDate**: (string) 
- **newestVersion**: (string) 
- **newestReleaseDate**: (string) 
- **canUpdate**: (boolean) 

### ~~download >*~~
**Not yet implemented**<br>
Download a (update) image. 

#### Parameters
- **(version)**: (string) Defaults to latest version available

### ~~install >~~
**Not yet implemented**<br>
Install a downloaded (update) image.

#### Parameters
- **(version)**: (string) Defaults to downloaded version.
- **(clean)**: (boolean) Whether settings on the printer should be cleared. Default: false.

### ~~clear >~~
**Not yet implemented**<br>
Remove all downloaded (update) images.

### ~~state <+~~
**Not yet implemented**<br>
Updating state update. Received when user connects to namespace or when it changes.

#### Contains
- **state**: (string) Options: 
  - *idle*
  - *downloading*
  - *download failed*
  - *download completed*: 
- **stateMessage**: Human readable message

### ~~progress <+~~
**Not yet implemented**<br>
Download progress. 

#### Contains
- **progress**: (number) 
- **imageSize**: (number)
-->
