var socketClient = require('socket.io-client');
var request = require('request');
var debug = require('debug')('cloud:test:mock:user');

module.exports = User;

function User(cloudURL) {
  if (!(this instanceof User)) return new User(cloudURL);

  this.cloudURL = cloudURL;

  this.key = "";

  this.register = function(callback) {
    var postOptions = {
      url:this.cloudURL+"/user/register",
      json:{} // makes sure the response is parsed as json
    };
    request.post(postOptions, function(err, httpResponse, body) {
      if(err) return callback(new Error("user register error: "+err),null);
      if(typeof body !== "object") {
        return callback(new Error("invalid register response"),null,null);
      }
      //body = JSON.parse(body);
      this.key = body.key;
      callback(null,body.key);
    });
  };
  this.connectTo = function(nspName,key,options,callback) {
    if(typeof options === "function") {
      callback = options;
      options = undefined;
    }
    if(options === undefined) {
      options = {};
    }
    var urlParams = options.urlParams || "";
    var nspURL = this.cloudURL+nspName+"?key="+key+urlParams;
    //debug("user:connectTo: '"+nspURL+"'",options);
    //options.transports=['polling'];
    var nsp = socketClient(nspURL,options);
    nsp.once('connect', function(){
      callback(null,nsp);
      nsp.removeListener('error',onError);
    });
    nsp.on('error', onError);
    function onError(err){
      callback(err,nsp);
    }
  };
}
