require("should");
require("mocha");
var debug = require('debug')('cloud:test:localprinters');

describe('/config', function () {
  var CLOUD_URL = "http://localhost:5000";
  //var CLOUD_URL = "http://cloud.doodle3d.com:5000";
  var user = require("./mock/user")(CLOUD_URL);
  var userKey;
  var userConfigNSP;
  
  before(function(done) {
    // Register new user
    user.register(function(err,newUserKey) {
      if(err) throw new Error(err);
      userKey = newUserKey;
      user.connectTo("/config",userKey,{forceNew:true},function(err,nsp) {
        if(err) throw new Error(err);
        userConfigNSP = nsp;
        done();
      });
    });
  });
  afterEach(function() {
    if(userConfigNSP) userConfigNSP.removeAllListeners();
    //if(printersRootNSP) printersRootNSP.removeAllListeners();
  });
  describe("getSlicers",function() {
    it("should return config",function(done) {
      userConfigNSP.emit("getSlicers",function(err,slicers){
        //debug("getSlicers response: ",JSON.stringify(slicers, null, 2));
        slicers.should.have.properties("curaEngine");        
        for(var slicerName in slicers) {
          var slicer = slicers[slicerName];
          slicer.should.have.properties("contentTypes");
        }
        slicers.curaEngine.should.have.properties("printerType","material","profile");
        slicers.curaEngine.printerType.should.be.type('object');
        for(var printerTypeName in slicers.curaEngine.printerType) {
          slicers.curaEngine.printerType[printerTypeName].should.be.type('string');
        }
        done();
      }); 
      userConfigNSP.on("error",function(err) {
        done(err.description);
      });
    });
    it("should not crash without callback",function(done) {
      userConfigNSP.emit("getSlicers"); 
      userConfigNSP.emit("getSlicers",done); 
      userConfigNSP.on("error",function(err) {
        done(err.description);
      });
    });
    it("should not crash with data and callback",function(done) {
      userConfigNSP.emit("getSlicers",{},function(err,slicers) {
        slicers.should.have.properties("curaEngine");
        done();
      }); 
      userConfigNSP.on("error",function(err) {
        done(err.description);
      });
    });
  });
});
