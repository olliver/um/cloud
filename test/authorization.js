require("should");
require("mocha");
var er = require("../lib/ws/ioErrors");
var debug = require('debug')('cloud:test:printer');
var config = require('../lib/config');
var async = require('async');
var yaml = require('read-yaml').sync;
var PORT = process.env.PORT ? process.env.PORT : config.PORT;

describe('/printer', function () {
  var CLOUD_URL = "http://localhost:"+PORT;
  //var CLOUD_URL = "https://cloud.doodle3d.com";
  var printer = require("./mock/printer")(CLOUD_URL);
  var user = require("./mock/user")(CLOUD_URL);
  var userKey;
  var printerID;
  var printerKey;
  var userPrinterNSP;
  var printerRootNSP;
  
  function userDisconnect(callback) {
    if(userPrinterNSP) userPrinterNSP.disconnect();
    if(callback) process.nextTick(callback);
  }
  function userConnect(urlParams,callback) {
    if(typeof urlParams  === "function") {
      callback = urlParams;
      urlParams = "";
    }
    user.connectTo("/"+printerID+"-printer",userKey,{forceNew:true,urlParams:urlParams},function(err,nsp) {
      if(err) {
        if(callback) callback(err);
        return;
      }
      userPrinterNSP = nsp;
      if(callback) process.nextTick(callback);
    });
  }
  
  before(function(done) {
    // Register new printer
    printer.register(function(err,key,id){
      if(err) throw new Error(err);
      printerID = id;
      printerKey = key;
      
      // Register new user
      user.register(function(err,newUserKey) {
        if(err) throw new Error(err);
        userKey = newUserKey;
        
        // connect printer to / (so that namespaces are created)
        printer.connectTo("/",printerKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          printerRootNSP = nsp;
          
          setTimeout(done,300); 
        });
      });
    });
  });
  afterEach(function() {
    if(userPrinterNSP) userPrinterNSP.removeAllListeners();
    if(printerRootNSP) printerRootNSP.removeAllListeners();
  });
  
  describe("Authorize user to local printer",function() {
    beforeEach(function(done) {
      userDisconnect(done);
    });
    it('should be allowed access to local printer', function (done) {
      userConnect(done);
    });
    it('should not be allowed access to local printer', function (done) {
      userConnect("&forceremote=1",function(err) {
        debug("user connect response: ",err);
        debug("  typeof err",typeof err);
        if(err) done();
      });
    });
  });
  describe("Authorize user to preferred printer",function() {
    before(function(done) {
      // Connect to namespace /printers
      user.connectTo("/printers",userKey,{forceNew:true},function(err,nsp) {
        if(err) throw new Error(err);
        nsp.emit("add",{id:printerID},function(err,responseData) {
          done(err); 
          nsp.disconnect();
        });
      });
    });
    beforeEach(function(done) {
      userDisconnect(done);
    });
    it('should be allowed access to preferred printer', function (done) {
      userConnect(done);
    });
    it('should be allowed access to preferred remote printer', function (done) {
      userConnect("&forceremote=1",done);
    });
  });
  after(function(done) {
    if(userPrinterNSP) userPrinterNSP.disconnect();
    if(printerRootNSP) printerRootNSP.disconnect();
    setTimeout(done,100);
  });
});
