require("should");
require("mocha");
var er = require("../lib/ws/ioErrors");
var debug = require('debug')('cloud:test:printer');
var config = require('../lib/config');
var async = require('async');
var yaml = require('read-yaml').sync;
var PORT = process.env.PORT ? process.env.PORT : config.PORT;

describe('/printer', function () {
  var CLOUD_URL = "http://localhost:"+PORT;
  //var CLOUD_URL = "https://cloud.doodle3d.com";
  var printer = require("./mock/printer")(CLOUD_URL);
  var user = require("./mock/user")(CLOUD_URL);
  var userKey;
  var printerID;
  var printerKey;
  var userPrinterNSP;
  var printerRootNSP;
  var printerPrinterNSP;
  
  function printerDisconnect(callback) {
    printerPrinterNSP.disconnect();
    printerRootNSP.disconnect();
    //if(callback) process.nextTick(callback);
    if(callback) setTimeout(callback,100);
  }
  function printerConnect(callback) {
    printer.connectTo("/"+printerID+"-printer",printerKey,{forceNew:true},function(err,nsp) {
      if(err) throw new Error(err);
      printerPrinterNSP = nsp;
      if(callback) setTimeout(callback,100);
    });
  }
  function userDisconnect(callback) {
    userPrinterNSP.disconnect();
    if(callback) process.nextTick(callback);
  }
  function userConnect(callback) {
    user.connectTo("/"+printerID+"-printer",userKey,{forceNew:true},function(err,nsp) {
      if(err) throw new Error(err);
      userPrinterNSP = nsp;
      if(callback) process.nextTick(callback);
    });
  }
  
  before(function(done) {
    // Register new printer
    printer.register(function(err,key,id){
      if(err) throw new Error(err);
      printerID = id;
      printerKey = key;
      
      // Register new user
      user.register(function(err,newUserKey) {
        if(err) throw new Error(err);
        userKey = newUserKey;
        
        // connect printer to / (so that namespaces are created)
        printer.connectTo("/",printerKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          printerRootNSP = nsp;
          
          process.nextTick(function() {
            userConnect(function() {
              printerConnect(done);
            });
          });
        });
      });
    });
  });
  afterEach(function() {
    if(userPrinterNSP) userPrinterNSP.removeAllListeners();
    if(printerRootNSP) printerRootNSP.removeAllListeners();
    if(printerPrinterNSP) printerPrinterNSP.removeAllListeners();
  });
  
  describe("Events from printer to client",function() {
    var stateValue = "printing";
    
    it('should be forwarded in /{prid}-printer namespace', function (done) {
      userPrinterNSP.once("state",function(data) {
        debug("usersPrinterNSP: onState data:",data);
        data.should.have.keys(["state"]);
        data.state.should.equal(stateValue);
        done();
      });
      printerPrinterNSP.emit("state", {state: stateValue}); 
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
    });

    it('should inform new socket clients about current state from cache', function (done) {
       // Register new user
      user.register(function(err,newUserKey) {
        if(err) throw new Error(err);
        var userKey = newUserKey;
        
        // connect user to /{prid}-printer
        var nspName = "/"+printerID+"-printer";
        user.connectTo(nspName,userKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          var userPrinterNSP = nsp;
          userPrinterNSP.once("state",function(data) {
            debug("usersPrinterNSP: onState data:",data);
            data.should.have.keys(["state"]);
            data.state.should.equal(stateValue);
            done();
          });
          userPrinterNSP.on("error",function(err) {
            done(err.description);
          });
        });
      });
    });
    
    it('should not cache events without data', function (done) {
       // Register new user
      printerPrinterNSP.emit("empty"); 
      user.register(function(err,newUserKey) {
        if(err) throw new Error(err);
        var userKey = newUserKey;
        
        // connect user to /{prid}-printer
        var nspName = "/"+printerID+"-printer";
        user.connectTo(nspName,userKey,{forceNew:true},function(err,nsp) {
          debug("not cache:connected");
          if(err) throw new Error(err);
          var userPrinterNSP = nsp;
          // 500 ms later I assume empty won't be received anymore
          setTimeout(done,500);
          userPrinterNSP.once("empty",function(data) {
            throw new Error("should not have cached and emitted empty event");
          });
          userPrinterNSP.on("error",function(err) {
            done(err.description);
          });
        });
      });
    });
    describe("multiple printers connections",function() {
      var printerPrinterNSP2;
      before(function(done) {
        // connect as printer again
        printer.connectTo("/"+printerID+"-printer",printerKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          printerPrinterNSP2 = nsp;
          done();
        });
      });
      it('should not send event from first connected printer back to printer', function (done) {
        printerPrinterNSP.once("stop",function() {
          done(new Error("printer should not recieve it's own event"));
        });
        printerPrinterNSP2.once("stop",function() {
          done(new Error("new printer should not recieve events from older printer connection"));
        });
        printerPrinterNSP.emit("stop");
        setTimeout(done,200);
      });
      it('should not send event from last connected printer back to printer', function (done) {
        printerPrinterNSP.once("stop",function() {
          debug("printer1 stopped");
          done(new Error("first printer connection should not recieve events from new printer connection"));
        });
        printerPrinterNSP2.once("stop",function() {
          debug("printer2 stopped");
          done(new Error("printer should not recieve it's own event"));
        });
        printerPrinterNSP2.emit("stop");
        setTimeout(done,200);
      });
      after(function(done) {
        printerPrinterNSP2.disconnect();
        printerConnect(done);
      });
    });
  });
  
  describe("Events from clients to printer",function() {
    it('should be forwarded in /{prid}-printer namespace (without arguments)', function (done) {
      printerPrinterNSP.once("stop",function() {
        done();
      });
      userPrinterNSP.emit("stop"); 
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
    });
    
    it('should be forwarded in /{prid}-printer namespace with data', function (done) {
      var printUrl = "https://www.youmagine.com/documents/8826/download";
      printerPrinterNSP.once("print",function(data) {
        data.should.have.properties(["url"]);
        data.url.should.equal(printUrl);
        done();
      });
      printerPrinterNSP.on("error",function(err) {
        done(err.description);
      });
      userPrinterNSP.emit("print",{url:printUrl}); 
    });
    
    it('should be forwarded in /{prid}-printer namespace with data and callback', function (done) {
      var printUrl = "https://www.youmagine.com/documents/8826/download";
      var errMessage = "Missing something argument";
      printerPrinterNSP.once("print",function(data,callback) {
        //debug("print: ",data,callback);
        data.should.have.properties(["url"]);
        data.url.should.equal(printUrl);
        callback({err:new er.InvalidArgumentsError(errMessage)});
      });
      printerPrinterNSP.on("error",function(err) {
        done(err.description);
      });
      userPrinterNSP.emit("print",{url:printUrl},function(response) {
        //debug("print response: ",response);
        response.should.have.keys(["err"]);
        response.err.should.have.keys(["name","message"]);
        response.err.name.should.equal("InvalidArgumentsError");
        response.err.message.should.equal(errMessage);
        done();
      });
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
    });
    
    it('should be forwarded in /{prid}-printer namespace with data and callback without error response', function (done) {
      printerPrinterNSP.once("stop",function(data,callback) {
        //debug("print: ",data,callback);
        callback(null,"ok");
      });
      printerPrinterNSP.on("error",function(err) {
        done(err.description);
      });
      userPrinterNSP.emit("stop",{},function(err,response) {
        //debug("print response: ",err,response);
        (err === null).should.equal(true);
        response.should.equal("ok");
        done();
      });
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
    });
    
    it('should be forwarded in /{prid}-printer namespace with callback only', function (done) {
      var errMessage = "Missing url argument";
      printerPrinterNSP.once("stop",function(data,callback) {
        //debug("stop: ",data,callback);
        if(typeof data === 'function') callback = data;
        callback({err:new er.InvalidArgumentsError(errMessage)});
      });
      printerPrinterNSP.on("error",function(err) {
        done(err.description);
      });
      userPrinterNSP.emit("stop",function(response) {
        //debug("print response: ",response);
        response.should.have.keys(["err"]);
        response.err.should.have.keys(["name","message"]);
        response.err.name.should.equal("InvalidArgumentsError");
        response.err.message.should.equal(errMessage);
        done();
      });
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
    });
    describe("multiple printers connections",function() {
      var printerPrinterNSP2;
      before(function(done) {
        // connect as printer again
        printer.connectTo("/"+printerID+"-printer",printerKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          printerPrinterNSP2 = nsp;
          done();
        });
      });
      it('should send event to last connected printer when user sends event', function (done) {
        printerPrinterNSP.once("stop",function() {
          debug("printer1 stopped");
          done(new Error("older printer connection should not recieve event"));
        });
        printerPrinterNSP2.once("stop",function() {
          debug("printer2 stopped");
          done();
        });
        userPrinterNSP.emit("stop"); 
        userPrinterNSP.on("error",function(err) {
          done(err.description);
        });
      });
      after(function(done) {
        printerPrinterNSP2.disconnect();
        printerConnect(done);
      });
    });
  });
  
  describe("when printer and user connect multiple times",function() {
    beforeEach(function(done) {
      var tasks = [
                   printerDisconnect,
                   printerConnect,
                   userDisconnect,
                   userConnect,
        
                   printerDisconnect,
                   printerConnect,
                   printerDisconnect,
                   printerConnect,
        
                   printerDisconnect,
                   printerConnect,
                   userDisconnect,
                   userConnect,

                   userDisconnect,
                   userConnect,
                   userDisconnect,
                   userConnect,
                  ];
      async.series(tasks,done);
    });
    it('should forward event from printer to client once', function (done) {
      this.timeout(0);
      var stateEventCounter = 0;
      userPrinterNSP.on("state",function(data) {
        debug("  state: ",data);
        stateEventCounter++;
      });
      printerPrinterNSP.emit("state", {state: "stopping"}); 
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
      setTimeout(function() {
        debug("  stateEventCounter: ",stateEventCounter);
        if(stateEventCounter > 1) done(new Error("state event called multiple times"));
        else if(stateEventCounter === 0) done(new Error("state event not called at all"));
        else done();
      },1000);
    });
    it('should forward event from client to printer once', function (done) {
      this.timeout(0);
      var eventCounter = 0;
      printerPrinterNSP.on("stop",function(data) {
        debug("  stop: ",data);
        eventCounter++;
      });
      userPrinterNSP.emit("stop"); 
      printerPrinterNSP.on("error",function(err) {
        done(err.description);
      });
      setTimeout(function() {
        debug("  eventCounter: ",eventCounter);
        if(eventCounter > 1) done(new Error("print event called multiple times"));
        else if(eventCounter === 0) done(new Error("print event not called at all"));
        else done();
      },1000);
    });
  });
        
  describe("When a user in {prid}-printer sends print",function() {
    //var defaultConfig = yaml('lib/config/defaultConfig.yml');
    var printURL = "https://www.youmagine.com/documents/8826/download";
        
    it('should add settings when sending stl',function(done) {
      printerPrinterNSP.once("print",function(data) {
        data.should.have.keys(["url","settings","contentType"]);
        data.settings.should.containEql({
          layerThickness: 100,
          printSpeed: 50
        });
        done();
      });
      printerPrinterNSP.on("error",function(err) {
        done(err.description);
      });
      userPrinterNSP.emit("print",{ url:"https://www.youmagine.com/documents/8826/download",
                                   contentType:"stl"}); 
    });
    it('should add overriden settings when sending stl and defining overrides',function(done) {
      printerPrinterNSP.once("print",function(data) {
        data.should.have.keys(["url","settings","contentType","printerType","material","profile"]);
        data.settings.skirtDistance.should.equal(0);
        done();
      });
      printerPrinterNSP.on("error",function(err) {
        done(err.description);
      });
      userPrinterNSP.emit("print",{ url:"https://www.youmagine.com/documents/8826/download",
                                    contentType:"stl",
                                    printerType:"ultimaker2",
                                    material:"abs",
                                    profile:"normalQuality"}); 
    });
    it('should return error when providing invalid arguments',function(done) {
      userPrinterNSP.emit("print",{ contentType:"stl",
                                    printerType:["foo"]},function(err,data) {
        //debug("print response: ",err,data);
        if(err === undefined) return done("err should not be undefined");
        else if(err === null) return done("err should not be null");
        err.should.have.properties(["name","message","issues"]);
        err.issues.length.should.equal(1);
        err.issues[0].subject.should.equal("printerType");
        done();
      }); 
    });
    it('should return error when providing non existing profile',function(done) {
      
      userPrinterNSP.emit("print",{ contentType:"stl",
                                    url: printURL,
                                    profile:'fake'},function(err,data) {
        if(err === undefined) return done("err should not be undefined");
        else if(err === null) return done("err should not be null");
        err.should.have.properties(["name","message"]);
        done();
      }); 
    });
    it('should return error when not providing url or stream',function(done) {
      userPrinterNSP.emit("print",{ contentType:"stl"},function(err,data) {
        if(err === undefined) return done("err should not be undefined");
        else if(err === null) return done("err should not be null");
        err.should.have.properties(["name","message"]);
        done();
      }); 
    });
    it('should take stl as default when sending print',function(done) {
      printerPrinterNSP.once("print",function(data) {
        data.should.have.keys(["url","settings","contentType"]);
        data.contentType.should.equal('stl');
        data.settings.should.containEql({
          layerThickness: 100,
          printSpeed: 50
        });
        done();
      });
      printerPrinterNSP.on("error",function(err) {
        done(err.description);
      });
      userPrinterNSP.emit("print",{ url:"https://www.youmagine.com/documents/8826/download"}); 
    });
    it('should override settings from profiles with specified settings',function(done) {
      printerPrinterNSP.once("print",function(data) {
        data.should.have.keys(["url","settings","contentType","material","profile"]);
        data.settings.layerThickness.should.equal(300);
        data.settings.skirtDistance.should.equal(0);
        done();
      });
      printerPrinterNSP.on("error",function(err) {
        done(err.description);
      });
      userPrinterNSP.emit("print",{ url:"https://www.youmagine.com/documents/8826/download",
                                    material:"abs",
                                    profile:"normalQuality",
                                    settings:{layerThickness:300}}); 
    });
  });

  describe("Server events per nsp (like online)",function() {
    before(printerDisconnect);
    it('should inform new socket clients from cache (in prid namespace) (when offline)', function (done) {
      user.register(function(err,newUserKey) {
        if(err) throw new Error(err);
        var nspName = "/"+printerID; // connect user to /{prid}
        user.connectTo(nspName,newUserKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          nsp.once("online",function(data) {
            //debug("usersPrinterNSP: online data:",data);
            data.should.have.eql({online:false});
            done();
          });
        });
      });
    });
    it('should update users when printer comes online', function (done) {
      userPrinterNSP.once("online",function(data) {
        //debug("usersPrinterNSP: online data:",data);
        data.should.have.eql({online:true});
        done();
      });
      printerConnect();
    });
    it('should inform new socket clients from cache (in feature namespace)', function (done) {
      user.register(function(err,newUserKey) {
        if(err) throw new Error(err);
        var nspName = "/"+printerID+"-printer"; // connect user to /{prid}-printer
        user.connectTo(nspName,newUserKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          nsp.once("online",function(data) {
            //debug("usersPrinterNSP: online data:",data);
            data.should.have.eql({online:true});
            done();
          });
        });
      });
    });
    it('should inform new socket clients from cache (in prid namespace)', function (done) {
      user.register(function(err,newUserKey) {
        if(err) throw new Error(err);
        var nspName = "/"+printerID; // connect user to /{prid}
        user.connectTo(nspName,newUserKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          nsp.once("online",function(data) {
            //debug("usersPrinterNSP: online data:",data);
            data.should.have.eql({online:true});
            done();
          });
        });
      });
    });
    it('should update users when printer goes offline', function (done) {
      setTimeout(function() { // wait so that we don't catch the previous online update
        userPrinterNSP.once("online",function(data) {
          //debug("usersPrinterNSP: online data:",data);
          data.should.have.eql({online:false});
          done();
        });
        printerDisconnect();
      },500);
    });
    it('should inform new socket clients from cache (in prid namespace) (when updated to offline)', function (done) {
      printerDisconnect(function() {
        user.register(function(err,newUserKey) {
          if(err) throw new Error(err);
          var nspName = "/"+printerID; // connect user to /{prid}
          user.connectTo(nspName,newUserKey,{forceNew:true},function(err,nsp) {
            if(err) throw new Error(err);
            nsp.once("online",function(data) {
              //debug("usersPrinterNSP: online data:",data);
              data.should.have.eql({online:false});
              done();
            });
          });
        });
      });
    });
  });
});
