require("should");
require("mocha");
var userConfig = require("../lib/config/userConfig")();
var debug = require('debug')('cloud:test:config');
var yaml = require('read-yaml').sync;

describe('userConfig', function () {
  var defaultConfig = yaml('lib/config/defaultConfig.yml');
  var curaEngineDefaults = defaultConfig.curaEngine.defaults;
  describe('get slicer config',function() {
    describe("when providing invalid contentType",function() {
      it("should return errors",function(done) {
        userConfig.getSlicerConfig("bla",{},function(err,config) {
          (err !== undefined).should.equal(true);
          done();
        });
      });
    });
    describe.skip("when providing contentType gcode",function() {
      it("should return empty config",function(done) {
        userConfig.getSlicerConfig("gcode",{},function(err,config) {
          (err === null).should.equal(true);
          config.should.eql({});
          done();
        });
      });
    });
    it("should maintain default config when not receiving overrides",function(done) {
      userConfig.getSlicerConfig("stl",{},function(err,config) {
        //debug("response: ",err,config);
        config.layer_height.should.equal(curaEngineDefaults.layer_height);
        config.gcode_flavor.should.equal(curaEngineDefaults.gcode_flavor);
        done();
      });
    });
    describe("when providing invalid override group",function() {
      it("should return errors",function(done) {
        userConfig.getSlicerConfig("stl",{mach:'a'},function(err,config) {
          //debug(err,config);
          (err !== undefined).should.equal(true);
          done();
        });
      });
    });
    describe("when providing invalid override selection",function() {
      it("should return errors",function(done) {
        userConfig.getSlicerConfig("stl",{machine:'a'},function(err,config) {
          //debug(err,config);
          (err !== undefined).should.equal(true);
          done();
        });
      });
    });
    describe("when providing valid override selection",function() {
      it("should return overriden config",function(done) {
        userConfig.getSlicerConfig("stl",{profile:'lowQuality'},function(err,config) {
          //debug(err,config);
          var lowQualityConfig = defaultConfig.curaEngine.profile.lowQuality;
          config.layer_height.should.equal(lowQualityConfig.layer_height);
          done();
        });
      });
      it("should maintain not overriden config",function(done) {
        userConfig.getSlicerConfig("stl",{profile:'lowQuality'},function(err,config) {
          config.filament_flow.should.equal(curaEngineDefaults.filament_flow);
          done();
        });
      });
      it("should ignore name field",function(done) {
        userConfig.getSlicerConfig("stl",{profile:'lowQuality'},function(err,config) {
          (config.name === undefined).should.equal(true);
          done();
        });
      });
      it("should do multiple overrides",function(done) {
        userConfig.getSlicerConfig("stl",{material:'abs',profile:'highQuality'},function(err,config) {
          //debug(err,config);
          var absConfig = defaultConfig.curaEngine.material.abs;
          var highQualityConfig = defaultConfig.curaEngine.profile.highQuality;
          config.filament_flow.should.equal(absConfig.filament_flow);
          config.layer_height.should.equal(highQualityConfig.layer_height);
          done();
        });
      });
      it("should maintain default config when not receiving overrides after other requests",function(done) {
        userConfig.getSlicerConfig("stl",{},function(err,config) {
          //debug("response: ",err,config);
          config.layer_height.should.equal(curaEngineDefaults.layer_height);
          config.filament_flow.should.equal(curaEngineDefaults.filament_flow);
          done();
        });
      });
    });
  });
  describe("get slicer config encoded",function() {
    describe("when providing valid arguments",function() {
      it("should encode",function(done) {
        userConfig.getSlicerConfigEncoded("stl",{},{},function(err,config) {
          config.layerThickness.should.equal(100);
          config.printSpeed.should.equal(50);
          (config.gcodeFlavor === undefined).should.equal(true);
          (config.startCode === undefined).should.equal(false);
          config.startCode.should.not.equal('');
          //debug("getSlicerConfigEncoded: ",err,config);
          done();
        });
      });
      it("should encode for ultimaker2",function(done) {
        userConfig.getSlicerConfigEncoded("stl",{printerType:'ultimaker2'},{},function(err,config) {
          //debug("getSlicerConfigEncoded: ",err,config);
          (config.gcodeFlavor === undefined).should.equal(true);
          config.startCode.should.not.equal('');
          config.startCode.should.containEql('M190'); //bed heating
          config.startCode.should.containEql('M109'); // nozzle heating
          done();
        });
      });
      it("should encode for ultimaker",function(done) {
        userConfig.getSlicerConfigEncoded("stl",{printerType:'ultimaker'},{},function(err,config) {
          //debug("getSlicerConfigEncoded: ",err,config);
          (config.gcodeFlavor === undefined).should.equal(true);
          config.startCode.should.not.equal('');
          config.startCode.should.containEql(';M190'); // disabled bed heating
          config.startCode.should.containEql('M109'); // nozzle heating
          done();
        });
      });
      it("should encode for ultimaker2 ABS",function(done) {
        userConfig.getSlicerConfigEncoded("stl",{printerType:'ultimaker2',material:'abs'},{},function(err,config) {
          //debug("getSlicerConfigEncoded: ",err,config);
          config.skirtDistance.should.equal(0);
          done();
        });
      });
    });
  });
  describe("get user's slicer config",function() {
    it("should return config",function() {
      var slicers = userConfig.getSlicers();
      slicers.should.have.properties("curaEngine");
    });
  });
});
