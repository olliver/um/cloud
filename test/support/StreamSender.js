'use strict';

var debug   = require('debug')('cloud:test:StreamSender');
var async   = require('async');
var util = require("util");
var EventEmitter = require("events").EventEmitter;
var ss = require("socket.io-stream");
var fs = require('fs');

module.exports = StreamSender;

function StreamSender(name, broadcasterNSP, consumerNSP) {
  if (!(this instanceof StreamSender)) return new StreamSender(name, broadcasterNSP, consumerNSP);

  var testFolder = "test/tmp/";
  var readFilePath = 'test/assets/snapshot.jpg';
  var writeFilePath = testFolder+name+'.jpg';
  var _outgoingStream;
  
  var _orgOnRead;
  var _onReadArgs;
  var _orgEnd;
  var _readCounter = 0;
  
  var _self = this;
  ss(consumerNSP).once(name, function(stream) {
    debug(name+': received ');
    if(!stream) {
      debug(name+' no stream');
      throw new Error('No stream');
    }
    var args = Array.prototype.slice.call(arguments);
    args.unshift('received');
    _self.emit.apply(_self,args); 

    var writeStream = fs.createWriteStream(writeFilePath);
    writeStream.on('error',function(err) {
      debug(name+': writeStream error: ',err);
    });
    debug('start writing: ',writeFilePath);
    stream.pipe(writeStream);
    stream.on('end',function() {
      debug(name+': stream end');
      fs.exists(writeFilePath,function(exists) {
        debug(name+': fileCreated: ',exists);
        var args = Array.prototype.slice.call(arguments);
        args.unshift('end');
        _self.emit.apply(_self,args); 
      });
    });
    stream.on('error',function(err) {
       debug(name+': incoming stream error: ',err);
    });
  });
  
  this.send = function(pause,callback) {
    debug('sendSnapshot: ',name);
    async.series([
      _self.removeFiles, // remove files
      function(next) { // send snapshot 
        _outgoingStream = ss.createStream();
        _outgoingStream.on('error',function(err) {
           debug(name+': outgoing stream error: ',err);
        });
        debug(name+": emitting ");
        if(pause) _self.pauseStream();
        ss(broadcasterNSP).emit(name, _outgoingStream, callback);
        var readStream = fs.createReadStream(readFilePath);
        readStream.pipe(_outgoingStream);
        readStream.on('error',function(err) {
           debug(name+': readStream stream error: ',err);
        });
      }
    ]);
  };
  this.removeFiles = function(next) {
    fs.exists(testFolder,function(exists) {
      if(exists) {
        fs.unlink(writeFilePath,function(err) {
          next();
        });
      }
      else fs.mkdir(testFolder,function(err) {
        next();
      });
    });
  }
  this.pauseStream = function() {
    debug('pauseStream');
    _orgOnRead = _outgoingStream._onread;
    _orgEnd = _outgoingStream.end;
    _outgoingStream._onread = function() {
      debug('outgoingstream _onread: ',_readCounter);
      _onReadArgs = arguments;
      var self = this;
      if(_readCounter === 1) {
        _self.emit('flowing');
      } else {
        _orgOnRead.apply(this, arguments);
      }
      _readCounter++;
    };
    _outgoingStream.end = function() {};
  }
  this.resumeStream = function() {
    debug('resumeStream');
    _orgOnRead.apply(_outgoingStream, _onReadArgs);
    _orgEnd.call(_outgoingStream);
  }
}
util.inherits(StreamSender, EventEmitter);
