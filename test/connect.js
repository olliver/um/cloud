require("should");
require("mocha");
var debug = require('debug')('cloud:test:connect');
var socketClient = require('socket.io-client');

describe('connect', function () {
  var CLOUD_URL = "http://localhost:5000";
  var printer = require("./mock/printer")(CLOUD_URL);
  var user = require("./mock/user")(CLOUD_URL);
  var userKey;
  var printerKey;
  var userNSP;
  var printerNSP;
  
  before(function(done) {
    // Register new printer
    printer.name = "connect-printer";
    printer.register(function(err,key,id){
      if(err) throw new Error(err);
      printerKey = key;
      
      // Register new user
      user.register(function(err,newUserKey) {
        if(err) throw new Error(err);
        userKey = newUserKey;
        done();
      });
    });
  });
  afterEach(function(done) {
    setTimeout(function() {
      if(userNSP) {
        userNSP.removeAllListeners();
        userNSP.disconnect();
      }
      if(printerNSP) {
        printerNSP.removeAllListeners();
        printerNSP.disconnect();
      }
      setTimeout(done,200); 
    },200);
  });
  describe('when user connects with a valid key',function() {
    it("should connect",function(done) {
      userNSP = socketClient(CLOUD_URL+"?key="+userKey,{forceNew:true});
      userNSP.once('connect', function(){
        done();
      });
      userNSP.once('error', function(err){
        done(err);
      });
    });
  });
  
  describe('when user connects without key',function() {
    it("should not connect",function(done) {
      userNSP = socketClient(CLOUD_URL,{forceNew:true});
      userNSP.once('connect', function(){
        done("User without key should not be connected ");
      });
      userNSP.once('error', function(err){
        err.should.be.an.String;
        done();
      });
    });
  });
  
  describe('when user connects with invalid key',function() {
    it("should not connect",function(done) {
      userNSP = socketClient(CLOUD_URL+"?key=abc",{forceNew:true});
      userNSP.once('connect', function(){
        done("User with invalid key should not be connected ");
      });
      userNSP.once('error', function(err){
        err.should.be.an.String;
        done();
      });
    });
  });
  
  
  describe('when printer connects with a valid key',function() {
    it("should connect",function(done) {
      printerNSP = socketClient(CLOUD_URL+"?type=printer&key="+printerKey,{forceNew:true});
      printerNSP.once('connect', function(){
        done();
      });
      printerNSP.once('error', function(err){
        done(err);
      });
    });
  });
  
  describe('when printer connects without key',function() {
    it("should not connect",function(done) {
      nsp = socketClient(CLOUD_URL+"?type=printer",{forceNew:true});
      nsp.once('connect', function(){
        done("Printer without key should not be connected ");
      });
      nsp.once('error', function(err){
        err.should.be.an.String;
        done();
      });
    });
  });
  
  describe('when printer connects with invalid key',function() {
    it("should not connect",function(done) {
      nsp = socketClient(CLOUD_URL+"?type=printer&key=abc",{forceNew:true});
      nsp.once('connect', function(){
        done("Printer with invalid key should not be connected ");
      });
      nsp.once('error', function(err){
        err.should.be.an.String;
        done();
      });
    });
  });
  
});
