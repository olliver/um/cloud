require("should");
require("mocha");
var er = require("../lib/ws/ioErrors");
var debug = require('debug')('cloud:test:streaming');
var async = require('async');
var config = require('../lib/config');
var ss = require("socket.io-stream");
var fs = require('fs');
var PORT = process.env.PORT ? process.env.PORT : config.PORT;
var StreamSender = require('./support/StreamSender');

describe('streaming', function () {
  var CLOUD_URL = "http://localhost:"+PORT;
  //var CLOUD_URL = "https://cloud.doodle3d.com:"+PORT;
  //var CLOUD_URL = "https://cloud.doodle3d.com";
  var printer = require("./mock/printer")(CLOUD_URL);
  var user = require("./mock/user")(CLOUD_URL);
  var user2 = require("./mock/user")(CLOUD_URL);
  var userKey;
  var printerID;
  var printerKey;
  var userPrinterNSP;
  var printerRootNSP;
  var printerPrinterNSP;
  var userWebcamNSP;
  var user2WebcamNSP;
  var printerWebcamNSP;
  var testFolder = "test/tmp/";
  
  before(function(done) {
    // Register new printer
    printer.register(function(err,key,id){
      if(err) throw new Error(err);
      printerID = id;
      printerKey = key;
      
      // Register new user
      user.register(function(err,newUserKey) {
        if(err) throw new Error(err);
        userKey = newUserKey;
        
        // connect printer to / (so that namespaces are created)
        printer.connectTo("/",printerKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          printerRootNSP = nsp;
        
          // connect user to /{prid}-printer
          var nspName = "/"+printerID+"-printer";
          user.connectTo(nspName,userKey,{forceNew:true},function(err,nsp) {
            if(err) throw new Error(err);
            userPrinterNSP = nsp;

            // connect printer to /{prid}-printer
            printer.connectTo(nspName,printerKey,{forceNew:true},function(err,nsp) {
              if(err) throw new Error(err);
              printerPrinterNSP = nsp;
              
              done();
            });
          });
        });
      });
    });
  });
  afterEach(function() {
    if(userPrinterNSP) ss(userPrinterNSP).removeAllListeners();
    if(printerRootNSP) printerRootNSP.removeAllListeners();
    if(printerPrinterNSP) ss(printerPrinterNSP).removeAllListeners();
  });
  describe("Stream from clients to printer",function() {
    //this.timeout(100000);
    var fileName = "stanford_bunny_309_faces.stl";
    
    var testFolder = "test/tmp/";
    var filePath = testFolder+fileName;
    before(function(done) {
      fs.exists(testFolder,function(exists) {
        if(!exists) fs.mkdir(testFolder,done);
        else done();
      });
    });
    
    it('should be forwarded in /{prid}-printer namespace (without arguments)', function (done) {
      ss(printerPrinterNSP).once("stop",function(stream) {
        //debug("print: ");
        stream.pipe(fs.createWriteStream(filePath));
        stream.on('end',function() {
          fs.exists(filePath,function(exists) {
            if(exists) done();
            else done(new Error("file not found"));
          });
        });
      });
      var stream = ss.createStream();
      ss(userPrinterNSP).emit('stop', stream);
      fs.createReadStream("test/assets/"+fileName).pipe(stream);
      
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
    });
    
    it('should be forwarded in /{prid}-printer namespace with data', function (done) {
      ss(printerPrinterNSP).once("print",function(stream,data) {
        //debug("print: ",data);
        data.should.have.properties("contentType");
        data.contentType.should.equal("stl");
        stream.pipe(fs.createWriteStream(filePath));
        stream.on('end',function() {
          fs.exists(filePath,function(exists) {
            if(exists) done();
            else done(new Error("file not found"));
          });
        });
      });
      
      var stream = ss.createStream();
      ss(userPrinterNSP).emit('print', stream, {contentType:"stl"});
      fs.createReadStream("test/assets/"+fileName).pipe(stream);
      
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
      printerPrinterNSP.on("error",function(err) {
        done(err.description);
      });
      ss(printerPrinterNSP).on("error",function(err) {
        done(err.description);
      });
    });
    
    it('should be forwarded in /{prid}-printer namespace with data and callback', function (done) {
      ss(printerPrinterNSP).once("print",function(stream,data,callback) {
        //debug("print: ",data,callback);
        data.should.have.properties("contentType");
        data.contentType.should.equal("stl");
        stream.pipe(fs.createWriteStream(filePath));
        stream.on('end',function() {
          fs.exists(filePath,function(exists) {
            if(exists) callback(); //done();
            else done(new Error("file not found"));
          });
        });
      });
      
      var stream = ss.createStream();
      ss(userPrinterNSP).emit('print', stream, {contentType:"stl"}, function() {
        done();
      });
      fs.createReadStream("test/assets/"+fileName).pipe(stream);
      
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
    });
    
    it('should be forwarded in /{prid}-printer namespace with callback only', function (done) {
      ss(printerPrinterNSP).once("print",function(stream,callback) {
        //debug("print: ",callback);
        stream.pipe(fs.createWriteStream(filePath));
        stream.on('end',function() {
          fs.exists(filePath,function(exists) {
            if(exists) callback(); //done();
            else done(new Error("file not found"));
          });
        });
      });
      
      var stream = ss.createStream();
      ss(userPrinterNSP).emit('print', stream, function() {
        done();
      });
      fs.createReadStream("test/assets/"+fileName).pipe(stream);
      
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
    });
    
    after(function(done) {
      fs.unlink(filePath,function(err) {
        fs.rmdir(testFolder,done);
      });
    });
  });
  
  describe("Stream from printer to one client",function() {
    //this.timeout(10*1000);
    before(function(done) {
      // connect user to /{prid}-webcam
      var nspName = "/"+printerID+"-webcam";
      user.connectTo(nspName,userKey,{forceNew:true},function(err,nsp) {
        if(err) throw new Error(err);
        userWebcamNSP = nsp;
        printer.connectTo(nspName,printerKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          printerWebcamNSP = nsp;
          done();
        });
      });
    });
    it('should be forwarded in /{prid}-webcam namespace (without arguments)', function (done) {
      var sender = StreamSender('snapshot1',printerWebcamNSP,userWebcamNSP);
      sender.send();
      sender.on('end',function(exists) {
        fileCreated = exists;
        if(exists) done();
        else done(new Error("file not found"));
      });
    });
    
    it('should be blocked when there is already a streaming stream', function (done) {
      var fileCreated = false;
      var errorResponse = false;
      function checkDone() {
        debug('checkDone');
        if(fileCreated && errorResponse) done();
      }
      var sender = StreamSender('snapshot3a',printerWebcamNSP,userWebcamNSP);
      sender.send(true);
      sender.on('flowing', function() {
        // when stream is flowing we try sending another 
        // stream in the same namespace
        var sender2 = StreamSender('snapshot3b',printerWebcamNSP,userWebcamNSP);
        sender2.send(false, function(err) {
          debug('snapshot3b response: ',err);
          if(!err) return done("expected err");
          err.should.have.keys(["name","message"]);
          errorResponse = true;
          checkDone();
          sender.resumeStream();
        });
      });
      sender.on('end',function(exists) {
        fileCreated = exists;
        if(exists) done();
        else done(new Error("file not found"));
      });
    });
    it('should not be blocked when a previous stream finished', function (done) {
      var sender = StreamSender('snapshot4a',printerWebcamNSP,userWebcamNSP);
      sender.send(false);
      sender.on('end',function(exists) {
        if(exists) {
          var sender2 = StreamSender('snapshot4b',printerWebcamNSP,userWebcamNSP);
          sender2.send(false); 
          sender2.on('end',function(exists) {
            if(exists) done();
            else done(new Error("file not found"));
          });
        }
        else done(new Error("file not found"));
      });
    })
    it('should timeout a unfinished stream and accept a new one', function (done) {
      this.timeout(config.STREAM_TIMEOUT*2);
      var sender = StreamSender('snapshot5a',printerWebcamNSP,userWebcamNSP);
      sender.send(true);
      setTimeout(function() {
        var sender2 = StreamSender('snapshot5b',printerWebcamNSP,userWebcamNSP);
        sender2.send(false); 
        sender2.on('end',function(exists) {
          fileCreated = exists;
          if(exists) done();
          else done(new Error("file not found"));
        });
      },config.STREAM_TIMEOUT+1000);
    });
    afterEach(function() {
      ss(printerWebcamNSP).removeAllListeners();
    });
  });
  
  describe("Stream from printer to multiple clients",function() {
    //this.timeout(10*1000);
    var testFolder = "test/tmp/";
    var fileName = "image0.jpg";
    var filePath = testFolder+fileName;
    var filePath2 = testFolder+"2"+fileName;
    before(function(done) {
      // connect user to /{prid}-webcam
      var nspName = "/"+printerID+"-webcam";
      // var nspName = "/hello";
      debug("nspName: ",nspName);
      user.connectTo(nspName,userKey,{forceNew:true},function(err,nsp) {
        if(err) throw new Error(err);
        userWebcamNSP = nsp;
        // Register user 2
        user2.register(function(err,newUserKey) {
          if(err) throw new Error(err);
          var user2Key = newUserKey;
          // connect user2 to /{prid}-webcam
          user2.connectTo(nspName,user2Key,{forceNew:true},function(err,nsp) {
            debug("user2 connected ",err);
            if(err) throw new Error(err);
            user2WebcamNSP = nsp;
            // connect printer to /{prid}-printer
            printer.connectTo(nspName,printerKey,{forceNew:true},function(err,nsp) {
              debug("printer connected ",err);
              if(err) throw new Error(err);
              printerWebcamNSP = nsp;
              done();
            });
          });
        });
      });
    });
    beforeEach(function(done) {
      // create tmp files folder
      fs.exists(testFolder,function(exists) {
        if(exists) done();
        else fs.mkdir(testFolder,done);
      });
    });
    it('should be forwarded in /{prid}-webcam namespace (without arguments)', function (done) {  
      var sender = StreamSender('snapshot1',printerWebcamNSP,userWebcamNSP);
      sender.send();
      sender.on('end',function(exists) {
        fileCreated = exists;
        if(exists) done();
        else done(new Error("file not found"));
      });
    });
    it('should be blocked when there is already a streaming stream', function (done) {
      var fileCreated = false;
      var errorResponse = false;
      function checkDone() {
        debug('checkDone');
        if(fileCreated && errorResponse) done();
      }
      var sender = StreamSender('snapshot3a',printerWebcamNSP,userWebcamNSP);
      sender.send(true);
      sender.on('flowing', function() {
        // when stream is flowing we try sending another 
        // stream in the same namespace
        var sender2 = StreamSender('snapshot3b',printerWebcamNSP,userWebcamNSP);
        sender2.send(false, function(err) {
          debug('snapshot3b response: ',err);
          if(!err) return done("expected err");
          err.should.have.keys(["name","message"]);
          errorResponse = true;
          checkDone();
          sender.resumeStream();
        });
      });
      sender.on('end',function(exists) {
        fileCreated = exists;
        if(exists) done();
        else done(new Error("file not found"));
      });
    });
    it('should not be blocked when there a previous stream finished', function (done) {
      var sender = StreamSender('snapshot4a',printerWebcamNSP,userWebcamNSP);
      sender.send(false);
      sender.on('end',function(exists) {
        if(exists) {
          var sender2 = StreamSender('snapshot4b',printerWebcamNSP,userWebcamNSP);
          sender2.send(false); 
          sender2.on('end',function(exists) {
            if(exists) done();
            else done(new Error("file not found"));
          });
        }
        else done(new Error("file not found"));
      });
    });
    afterEach(function() {
      ss(printerWebcamNSP).removeAllListeners();
//      fs.unlink(filePath,function(err) {
//        fs.unlink(filePath2,function(err) {
//          fs.rmdir(testFolder,done);
//        });
//      });
    });
  });
  after(function(done) {
    fs.readdir(testFolder, function(err, files) {
//      if (files.length === 0) {
//        fs.rmdir(testFolder,done);
//      } else {
        async.each(files, function(fileName,next) {
          var filePath = testFolder + fileName;
          fs.unlink(filePath,next);
        }, function(err){
          if(err) done(err);
          else fs.rmdir(testFolder,done);
        });
//      }
    });
  });
});