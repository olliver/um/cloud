require("should");
require("mocha");
var debug = require('debug')('cloud:test:localprinters');

describe('/localprinters', function () {
  var CLOUD_URL = "http://localhost:5000";
  //var CLOUD_URL = "http://cloud.doodle3d.com:5000";
  //var CLOUD_URL = "https://cloud.doodle3d.com";
  var printer = require("./mock/printer")(CLOUD_URL);
  var user = require("./mock/user")(CLOUD_URL);
  var userKey;
  var printerID;
  var printerKey;
  var userLocalPrintersNSP; // user's socket connection to the /localprinters namespace
  var printerRootNSP; // printer's socket connection to the / namespace
  
  before(function(done) {
    // Register new printer
    printer.register(function(err,key,id){
      if(err) throw new Error(err);
      printerID = id;
      printerKey = key;
      
      // Register new user
      user.register(function(err,newUserKey) {
        if(err) throw new Error(err);
        userKey = newUserKey;
        done();
      });
    });
  });
  afterEach(function() {
    if(userLocalPrintersNSP) userLocalPrintersNSP.removeAllListeners();
    //if(printersRootNSP) printersRootNSP.removeAllListeners();
  });
  
  function reconnectUser(callback) {
    if(userLocalPrintersNSP) userLocalPrintersNSP.disconnect();
    user.connectTo("/localprinters",userKey,{forceNew:true},function(err,nsp) {
      userLocalPrintersNSP = nsp;
      callback(err,nsp);
    });
  }
  function connectPrinter(done) {
    // connect printer to / namespace
    printer.connectTo("/",printerKey,{forceNew:true},function(err,nsp) {
      if(err) throw new Error(err);
      printerRootNSP = nsp;
      // we need a slight delay between a printer's connect and disconnect
      // not sure why
      setTimeout(done,100);
    });
  }
  function disconnectPrinter() {
    // disconnect printer
    printerRootNSP.disconnect();
  }
  
  it('should list no localprinters when none are connected', function (done) {
    reconnectUser(function(err,nsp){
      userLocalPrintersNSP.once("list",function(data) {
        debug("list: data: ",data);
        data.should.have.keys(["printers"]);
        data.printers.should.have.length(0);
        done();
      });
      userLocalPrintersNSP.on("error",function(err) {
        done(err.description);
      });
    });
  });
  
  it('should connect printer', connectPrinter);
  
  it('should list 1 localprinter when one is connected', function (done) {
    reconnectUser(function(err,nsp){
      userLocalPrintersNSP.once("list",function(data) {
        debug("list: data: ",data);
        data.should.have.keys(["printers"]);
        data.printers.should.have.length(1);
        data.printers.forEach(function(printer) {
          printer.should.have.keys(["id","name","features","online"]);
          printer.id.should.equal(printerID,"invalid printer id");
          printer.name.should.equal(printer.name,"invalid printer name");
          printer.features.should.eql(printer.features,"invalid printer features");
          printer.online.should.eql(true,"printer shouldn't be offline");
        });
        done();
      });
      userLocalPrintersNSP.on("error",function(err) {
        done(err.description);
      });
    });
  });
  
//  it('should disconnect printer', disconnectPrinter);

  it('should trigger disappeared when printer goes offline', function (done) {
    userLocalPrintersNSP.on("disappeared",function(printer) {
      debug("disappeared: printer: ",printer);
      printer.should.have.keys(["id","name","features","online"]);
      printer.id.should.equal(printerID,"invalid printer id");
      printer.name.should.equal(printer.name,"invalid printer name");
      printer.features.should.eql(printer.features,"invalid printer features");
      printer.online.should.eql(false,"printer shouldn't be online");
      done();
    });
    userLocalPrintersNSP.on("error",function(err) {
      done(err.description);
    });
    
    disconnectPrinter();
  });
  
  //it('should connect printer', connectPrinter);
  
  it('should trigger list when printer comes online', function (done) {
    userLocalPrintersNSP.on("list",function(data) {
      debug("list: data: ",data);
      data.should.have.keys(["printers"]);
      if(data.printers.length > 0){
        data.printers.should.have.length(1);
        data.printers.forEach(function(printer) {
          printer.should.have.keys(["id","name","features","online"]);
          printer.id.should.equal(printerID,"invalid printer id");
          printer.name.should.equal(printer.name,"invalid printer name");
          printer.features.should.eql(printer.features,"invalid printer features");
          printer.online.should.eql(true,"printer shouldn't be offline");
        });
        done();
      }
    });
    userLocalPrintersNSP.on("error",function(err) {
      done(err.description);
    });
    
    connectPrinter();
  });
  
  it('should wait for a moment after connect before we disconnect printer', function(done){
    setTimeout(done,500);
  });
  
  it('should trigger list when printer goes offline', function (done) {
    userLocalPrintersNSP.once("list",function(data) {
      debug("list: data: ",data);
      data.should.have.keys(["printers"]);
      data.printers.should.have.length(0);
      data.printers.forEach(function(printer) {
        printer.should.have.keys(["id","name","features","online"]);
        printer.id.should.equal(printerID,"invalid printer id");
        printer.name.should.equal(printer.name,"invalid printer name");
        printer.features.should.eql(printer.features,"invalid printer features");
        printer.online.should.eql(true,"printer shouldn't be offline");
      });
      done();
    });
    userLocalPrintersNSP.on("error",function(err) {
      done(err.description);
    });
    
    disconnectPrinter();
  });
});
