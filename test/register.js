require("should");
require("mocha");
var er = require("../lib/ws/ioErrors");
var debug = require('debug')('cloud:test:register');
var config = require('../lib/config');
var request = require('request');
var PORT = process.env.PORT ? process.env.PORT : config.PORT;

var exampleInvalidArgumentsError = new er.InvalidArgumentsError();

describe('register', function () {
  var CLOUD_URL = "http://localhost:"+PORT;
  var name = "Ultimaker-871EFM";
  var features = ["printer","network","debug","update","config","webcam","slice"];
  
  describe("when register without arguments",function() {
    it('should respond with validation errors', function (done) {
      var postOptions = {
        url:CLOUD_URL+"/printer/register",
      };
      request.post(postOptions, function(err, httpResponse, body) {
        debug("register response: ",err,body);
        httpResponse.statusCode.should.equal(400);
        if(typeof body === "string") body = JSON.parse(body);
        body.should.have.key("err");
        body.err.should.have.properties("name","issues");
        body.err.name.should.equal(exampleInvalidArgumentsError.name);
        (body.err.issues).should.containDeep([{subject:'name'},
                                              {subject:'features'}]);
        done();
      });
    });
  });
  
  describe("when register with plain text",function() {
    it('should respond with validation errors', function (done) {
      var postOptions = {
        url:CLOUD_URL+"/printer/register",
        body:'{"name": "myprinter","features": ["a","b"]}'
      };
      request.post(postOptions, function(err, httpResponse, body) {
        debug("register response: ",err,body);
        httpResponse.statusCode.should.equal(400);
        if(typeof body === "string") body = JSON.parse(body);
        body.should.have.key("err");
        body.err.name.should.equal(exampleInvalidArgumentsError.name);
        (body.err.issues).should.containDeep([{subject:'name'},
                                              {subject:'features'}]);
        done();
      });
    });
  });
  
  describe("when register with invalid json",function() {
    it('should respond with syntax error', function (done) {
      var postOptions = {
        url:CLOUD_URL+"/printer/register",
        headers: {
          'content-type': 'application/json'
        },
        body:'{"name": "myprinter","features: ["a","b"]}'
      };
      request.post(postOptions, function(err, httpResponse, body) {
        debug("register response: ",err,body);
        httpResponse.statusCode.should.equal(400);
        body.indexOf("SyntaxError").should.equal(0);
        done();
      });
    });
  });
  
  describe("when register without name argument",function() {
    it('should respond with validation errors', function (done) {
      var postOptions = {
        url:CLOUD_URL+"/printer/register",
        json:{features: features}
      };
      request.post(postOptions, function(err, httpResponse, body) {
        debug("register response: ",err,body);
        httpResponse.statusCode.should.equal(400);
        if(typeof body === "string") body = JSON.parse(body);
        body.should.have.key("err");
        body.err.name.should.equal(exampleInvalidArgumentsError.name);
        // ToDo: check for name issue
        (body.err.issues).should.containDeep([{subject:'name'}]);
        done();
      });
    });
  });
  
  describe("when register with invalid features argument",function() {
    it('should respond with validation errors', function (done) {
      var postOptions = {
        url:CLOUD_URL+"/printer/register",
        json:{features: "a,b"}
      };
      request.post(postOptions, function(err, httpResponse, body) {
        debug("register response: ",err,body);
        httpResponse.statusCode.should.equal(400);
        if(typeof body === "string") body = JSON.parse(body);
        body.should.have.key("err");
        body.err.name.should.equal(exampleInvalidArgumentsError.name);
        // ToDo: check for feature issue
        (body.err.issues).should.containDeep([{subject:'features'}]);
        done();
      });
    });
  });
  
  describe("when register with valid arguments",function() {
    it('should respond without validation errors', function (done) {
      var postOptions = {
        url:CLOUD_URL+"/printer/register",
        json:{name:name,features: features}
      };
      request.post(postOptions, function(err, httpResponse, body) {
        debug("register response: ",err,body);
        httpResponse.statusCode.should.equal(200);
        if(typeof body === "string") body = JSON.parse(body);
        body.should.not.have.key("err");
        body.should.have.properties("id","key");
        done();
      });
    });
  });
  
  describe("when register user with valid arguments",function() {
    it('should respond without validation errors', function (done) {
      var postOptions = {
        url:CLOUD_URL+"/user/register"
      };
      request.post(postOptions, function(err, httpResponse, body) {
        debug("register response: ",err,body);
        httpResponse.statusCode.should.equal(200);
        if(typeof body === "string") body = JSON.parse(body);
        body.should.not.have.key("err");
        body.should.have.properties("id","key");
        done();
      });
    });
  });
  
});
