require("should");
require("mocha");
var debug = require('debug')('cloud:test:printers:onlineState');

describe('/Printers: online state', function () {
  var CLOUD_URL = "http://localhost:5000";
  var printer = require("../mock/printer")(CLOUD_URL);
  var user = require("../mock/user")(CLOUD_URL);
  var userKey;
  var printerID;
  var printerKey;
  var printersNSP;
  var printersRootNSP;
  
  before(function(done) {
    // Register new printer
    printer.register(function(err,key,id){
      if(err) throw new Error(err);
      printerID = id;
      printerKey = key;
      
      // Register new user
      user.register(function(err,newUserKey) {
        if(err) throw new Error(err);
        userKey = newUserKey;
        
        // Connect to namespace /printers
        user.connectTo("/printers",userKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          printersNSP = nsp;
          
          // Add printer to preferred printers of user
          printersNSP.emit("add",{id:printerID},function(err,responseData) {
            setTimeout(done,200);
          });
        });
      });
    });
  });
  afterEach(function() {
    if(printersNSP) printersNSP.removeAllListeners();
    if(printersRootNSP) printersRootNSP.removeAllListeners();
  });

  it("should list printer as offline when it's not connected yet", function (done) {
    // reconnect user to /printers namespace  
    printersNSP.disconnect();
    user.connectTo("/printers",userKey,{forceNew:true},function(err,nsp) {
      debug("user connected to /printers");
      if(err) throw new Error(err);
      printersNSP = nsp;
      printersNSP.once("appeared",function(data) {
        debug("printer appeared, online: ",data.online);
        data.online.should.equal(false);
        done();
      });
      printersNSP.once("error",function(err) {
        done(err.description);
      });
    });
  });
  it("should connect a printer", function (done) {
    // connect printer to / namespace
    printer.connectTo("/printers",printerKey,{forceNew:true},function(err,nsp) {
      if(err) throw new Error(err);
      printersRootNSP = nsp;
      // we need a slight delay between a printer's connect and disconnect
      // not sure why
      setTimeout(done,100);
    });
  });
  it("should list printer as online when it's connected", function (done) {
    // reconnect user to /printers namespace
    printersNSP.disconnect();
    user.connectTo("/printers",userKey,{forceNew:true},function(err,nsp) {
      if(err) throw new Error(err);
      printersNSP = nsp;
      printersNSP.once("appeared",function(data) {
        debug("printer appeared, online: ",data.online);
        data.online.should.equal(true);
        done();
      });
      printersNSP.once("error",function(err) {
        done(err.description);
      });
    });
  });
  it("should disconnect printer", function (done) {
    // disconnect printer from / namespace
    printersRootNSP.disconnect();
    done();
  });
  it("should list printer as offline when it's disconnected", function (done) {
    debug("user disconnected from /printers");
    printersNSP.disconnect();
    user.connectTo("/printers",userKey,{forceNew:true},function(err,nsp) {
      debug("user connected to /printers");
      if(err) throw new Error(err);
      printersNSP = nsp;
      printersNSP.once("appeared",function(data) {
        debug("printer appeared, online: ",data.online);
        data.online.should.equal(false);
        done();
      });
      printersNSP.once("error",function(err) {
        done(err.description);
      });
    });
  });
});
