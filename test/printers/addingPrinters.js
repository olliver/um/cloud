var should = require("should");
var debug = require('debug')('cloud:test:printers:addingPrinters');
require("mocha");

describe('/Printers: adding printers', function () {
  var CLOUD_URL = "http://localhost:5000";
  var printer = require("../mock/printer")(CLOUD_URL);
  var user = require("../mock/user")(CLOUD_URL);
  var userKey;
  var printerID;
  var printerKey;
  var printersNSP;
  var printersRootNSP;
  var printers = [];
  
  before(function(done) {
    // Register new printer
    printer.register(function(err,key,id){
      if(err) throw new Error(err);
      printerID = id;
      printerKey = key;
      
      // Register new user
      user.register(function(err,newUserKey) {
        if(err) throw new Error(err);
        userKey = newUserKey;
        
        // Connect to namespace /printers
        user.connectTo("/printers",userKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          printersNSP = nsp;
          done();
        });
      });
    });
  });
  afterEach(function(done) {
    if(printersNSP) printersNSP.removeAllListeners();
    if(printersRootNSP) printersRootNSP.removeAllListeners();

    function removeNext() {
      var printerID = printers.shift();
      debug("remove: ",printerID);
      printersNSP.emit("remove",{id:printerID},function(err,responseData) {
        debug("removed: ",printerID);
      });
      if(printers.length > 0) removeNext();
      else setTimeout(done,200); // slight delay to prevent catching remove events
    }
    if(printers.length > 0) removeNext();
    else done();
  });
  
  it('should list no printers for new users', function (done) {
    this.timeout(10000);
    printersNSP.once("list",function(data) {
//      debug("list: data: ",data);
//      debug("printers.length: ",printers.length);
      data.printers.should.have.length(0,"found printers");
      done();
    });
    printersNSP.on("error",function(err) {
      done(err.description);
    });
  });

  it('should trigger an appeared event when adding printer', function (done) {
    printersNSP.on("appeared",function(data) {
      debug("printer appeared, online: ",data.online);
      data.should.have.keys(["id","name","features","online"]);
      data.id.should.equal(printerID,"invalid printer id");
      data.name.should.equal(printer.name,"invalid printer name");
      data.features.should.eql(printer.features,"invalid printer features");
      data.online.should.eql(false,"printer shouldn't be online");
      done();
    });
    printersNSP.on("error",function(err) {
      done(err.description);
    });
    printers.push(printerID);
    printersNSP.emit("add",{id:printerID},function(err,responseData) {
      if(err) done(err);
    });
  });

  it('should trigger a list event when adding printer', function (done) {
    debug("should trigger a list event when adding printer");
    var printerID;

    printer.register(function(err,key,id){
      if(err) throw new Error(err);
      debug("registered new printer: ",id);
      printerID = id;
      printers.push(printerID);
      printersNSP.emit("add",{id:id},function(err,responseData) {
        if(err) done(err);
        debug("added new printer: ",id);
      });
      printersNSP.on("list",function(data) {
        debug("list: ",data);
        data.should.have.key("printers");
        data.printers.length.should.not.equal(0,"0 printers found");
        data.printers.forEach(function(printer) {
          printer.should.have.keys(["id","name","features","online"]);
          printer.id.should.equal(printerID,"invalid printer id");
          printer.name.should.equal(printer.name,"invalid printer name");
          printer.features.should.eql(printer.features,"invalid printer features");
          printer.online.should.eql(false,"printer shouldn't be online");
        });
        done();
      });
      printersNSP.on("error",function(err) {
        done(err.description);
      });
    });
  });

  it('should trigger the callback when available', function (done) {
    printersNSP.on("error",function(err) {
      done(err.description);
    });
    printer.register(function(err,key,id){
      if(err) throw new Error(err);
      printers.push(id);
      printersNSP.emit("add",{id:id},function(err,responseData) {
        should(err).equal(null);
        done();
      });
    });
  });

  it('should respond with error when adding the same printer to the same user', function (done) {
    printer.register(function(err,key,id){
      if(err) throw new Error(err);
      printers.push(id);
      printersNSP.emit("add",{id:id},function(err,responseData) {
        //debug("first add callback ",err,responseData);
        if(err) done(err);
        printersNSP.emit("add",{id:id},function(err,responseData) {
          //debug("second add callback ",err,responseData);
          if(err === undefined) return done("err should not be undefined");
          else if(err === null) return done("err should not be null");
          err.should.have.keys(["name","message"]);
          err.message.should.equal("Printer was already added");
          done();
        });
      });
      printersNSP.on("error",function(err) {
        done(err.description);
      });
    });
  });

  it('should respond with error when adding printer, without arguments', function (done) {
    debug("test");
    printersNSP.emit("add",function(err,responseData) {
      debug("add callback: ",err,responseData);
      
      if(err === undefined) return done("err should not be undefined");
      else if(err === null) return done("err should not be null");
      err.should.have.properties(["name","message"]);
      err.name.should.equal("InvalidArgumentsError");
      done();
    });
    printersNSP.on("error",function(err) {
      done(err.description);
    });
  });
  
  it('should respond with error when adding printer, without id argument', function (done) {
    printersNSP.emit("add",{},function(err,responseData) {
//      debug("add callback: ",err,responseData);
      
      if(err === undefined) return done("err should not be undefined");
      else if(err === null) return done("err should not be null");
      err.should.have.properties(["name","message"]);
      err.name.should.equal("InvalidArgumentsError");
      done();
    });
    printersNSP.on("error",function(err) {
      done(err.description);
    });
  });

  it('should respond with error when adding with invalid id argument', function (done) {
    printersNSP.emit("add",{id:"fake"},function(err,responseData) {
//      debug("add callback: ",err,responseData);
      if(err === undefined) return done("err should not be undefined");
      else if(err === null) return done("err should not be null");
      err.should.have.properties(["name","message"]);
      done();
    });
    printersNSP.on("error",function(err) {
      done(err.description);
    });
  });

  it('should respond with error when adding with invalid arguments', function (done) {
    printersNSP.emit("add","fake",function(err,responseData) {
//      debug("add callback: ",err,responseData);
      if(err === undefined) return done("err should not be undefined");
      else if(err === null) return done("err should not be null");
      err.should.have.properties(["name","message"]);
      done();
    });
    printersNSP.on("error",function(err) {
      done(err.description);
    });
  });

  it('should respond with error when adding non existing printer', function (done) {
    printersNSP.emit("add",{id:"53fe1c3a4d46990000c2fcc9"},function(err,responseData) {
//      debug("add callback: ",err,responseData);
      if(err === undefined) return done("err should not be undefined");
      else if(err === null) return done("err should not be null");
      err.should.have.properties(["name","message"]);
      done();
    });
    printersNSP.on("error",function(err) {
      done(err.description);
    });
  });

  it('should not crash when adding printer without callback', function (done) {
    printersNSP.emit("add",{});
    printersNSP.emit("add",{},function() {
      done();
    });
    printersNSP.on("error",function(err) {
      done(err.description);
    });
  });
  describe("When trying to add the same", function() {
    var printerID;
    
    before(function(done) {
      printer.register(function(err,key,id){
        if(err) throw new Error(err);
        printerID = id;
        printers.push(printerID);
        printersNSP.emit("add",{id:id},function(err,responseData) {
          if(err) done(err);
          printersNSP.emit("add",{id:id},function(err,responseData) {
            // ignoring error here
            done();
          });
        });
      });
    });
    
    it('should not save it multiple times', function (done) {
      printersNSP.disconnect();
      user.connectTo("/printers",userKey,{forceNew:true},function(err,nsp) {
        debug("user connected to /printers");
        if(err) throw new Error(err);
        printersNSP = nsp;
        printersNSP.on("list",function(data) {
          debug("list: ",data);
          data.should.have.key("printers");
          data.printers.length.should.equal(1);
          done();
        });
      });
    });
  });
  
  describe("When adding multiple printers", function() {
    
    beforeEach(function(done) {
      function addPrinter(callback) {
        printer.register(function(err,key,id){
          if(err) done(err);
          printers.push(printerID);
          printersNSP.emit("add",{id:id},function(err,responseData) {
            if(err) done(err);
            debug("added printer: ",id);
            if(callback) callback();
          });
        });
      }
      addPrinter(function() {
        addPrinter(function() {
          addPrinter(function() {
            done();
          });
        }); 
      });
    });
    
    it('should see multiple in list', function (done) {
      printersNSP.disconnect();
      user.connectTo("/printers",userKey,{forceNew:true},function(err,nsp) {
        if(err) throw new Error(err);
        printersNSP = nsp;
        printersNSP.on("list",function(data) {
          data.should.have.key("printers");
          data.printers.length.should.equal(printers.length);
          done();
        });
      });
    });
    
    it('should get multiple appeared', function (done) {
      printersNSP.disconnect();
      user.connectTo("/printers",userKey,{forceNew:true},function(err,nsp) {
        if(err) throw new Error(err);
        var numAppeared = 0;
        nsp.on("appeared",function(data) {
          numAppeared++;
          if(numAppeared === printers.length) done();
        });
      });
    });
  });
  
});
