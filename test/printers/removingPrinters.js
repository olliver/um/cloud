require("should");
require("mocha");
var debug = require('debug')('cloud:test:printers:removingPrinters');

describe('/Printers: removing printers', function () {
  var CLOUD_URL = "http://localhost:5000";
  var printer = require("../mock/printer")(CLOUD_URL);
  var user = require("../mock/user")(CLOUD_URL);
  var userKey;
  var printerID;
  var printerKey;
  var printersNSP;
  var printersRootNSP;
  
  before(function(done) {
    // Register new user
    user.register(function(err,newUserKey) {
      if(err) throw new Error(err);
      userKey = newUserKey;
      // Connect to namespace /printers
      user.connectTo("/printers",userKey,{forceNew:true},function(err,nsp) {
        if(err) throw new Error(err);
        printersNSP = nsp;
        done();
      });
    });
  });
  beforeEach(function(done) {
    printer.register(function(err,key,id){
      if(err) throw new Error(err);
      printerID = id;
      printersNSP.emit("add",{id:id},function() {
        setTimeout(done,200);
      });
    });
  });
  afterEach(function() {
    if(printersNSP) printersNSP.removeAllListeners();
    if(printersRootNSP) printersRootNSP.removeAllListeners();
  });

  function removePrinter(callback){
    printersNSP.emit("remove",{id:printerID},function(err,responseData) {
      if(callback) {
        callback(err,responseData);
      } else {
        if(err) throw new Error(err);
      }
    });
  }

  it('should trigger a disappeared event after removing a printer', function (done) {
    printersNSP.on("disappeared",function(data) {
      data.id.should.equal(printerID);
      data.should.not.have.keys(["_id","key"]);
      done();
    });
    printersNSP.once("error",function(err) {
      done(err.description);
    });
    removePrinter();
  });

  it('should trigger a list event', function (done) {
    printersNSP.on("list",function(data) {
      data.should.have.key("printers");
      data.printers.length.should.equal(0,"more than 0 printers found");
      done();
    });
    printersNSP.once("error",function(err) {
      done(err.description);
    });
    removePrinter();
  });

  it('should trigger the callback', function (done) {
    printersNSP.once("error",function(err) {
      done(err.description);
    });
    removePrinter(function(){
      done();
    });
  });

  it('should not crash when removing printer without callback', function (done) {
    printersNSP.emit("remove",{});
    printersNSP.emit("remove",{},function() {
      done();
    });
    printersNSP.on("error",function(err) {
      done(err.description);
    });
  });

  it('should respond with error when removing the same printer to the same user', function (done) {
    removePrinter(function() {
      removePrinter(function(err,responseData) {
        debug("response: ",err,responseData);
        if(err === undefined) return done("err should not be undefined");
        else if(err === null) return done("err should not be null");
        err.should.have.properties(["name","message"]);
        done();
      });
    });
    printersNSP.on("error",function(err) {
      done(err.description);
    });
  });

  it('should respond with error when removing printer, without id argument', function (done) {
    printersNSP.emit("remove",{},function(err,responseData) {
      debug("response: ",err,responseData);
      if(err === undefined) return done("err should not be undefined");
      else if(err === null) return done("err should not be null");
      err.should.have.properties(["name","message"]);
      err.name.should.equal("InvalidArgumentsError");
      done();
    });
    printersNSP.on("error",function(err) {
      done(err.description);
    });
  });
  
  it('should respond with error when removing printer, without arguments', function (done) {
    printersNSP.emit("remove",function(err,responseData) {
      debug("response: ",err,responseData);
      if(err === undefined) return done("err should not be undefined");
      else if(err === null) return done("err should not be null");
      err.should.have.properties(["name","message"]);
      err.name.should.equal("InvalidArgumentsError");
      done();
    });
    printersNSP.on("error",function(err) {
      done(err.description);
    });
  });

  it('should respond with error when removing with invalid id argument', function (done) {
    printersNSP.emit("remove",{id:"fake"},function(err,responseData) {
      debug("response: ",err,responseData);
      if(err === undefined) return done("err should not be undefined");
      else if(err === null) return done("err should not be null");
      err.should.have.properties(["name","message"]);
      done();
    });
    printersNSP.on("error",function(err) {
      done(err.description);
    });
  });

  it('should respond with error when removing with invalid arguments', function (done) {
    printersNSP.emit("remove","fake",function(err,responseData) {
      debug("response: ",err,responseData);
      if(err === undefined) return done("err should not be undefined");
      else if(err === null) return done("err should not be null");
      err.should.have.properties(["name","message"]);
      done();
    });
    printersNSP.on("error",function(err) {
      done(err.description);
    });
  });

  it('should respond with error when removing non existing printer', function (done) {
    printersNSP.emit("remove",{id:"53fe1c3a4d46990000c2fcc9"},function(err,responseData) {
      debug("response: ",err,responseData);
      if(err === undefined) return done("err should not be undefined");
      else if(err === null) return done("err should not be null");
      err.should.have.properties(["name","message"]);
      done();
    });
    printersNSP.on("error",function(err) {
      done(err.description);
    });
  });
});
