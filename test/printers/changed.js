require("should");
require("mocha");
var debug = require('debug')('cloud:test:printers:changed');

describe('/Printers: changed event', function () {
  var CLOUD_URL = "http://localhost:5000";
  var printer = require("../mock/printer")(CLOUD_URL);
  var user = require("../mock/user")(CLOUD_URL);
  var userKey;
  var printerID;
  var printerKey;
  var printersNSP;
  var printersRootNSP;
  
  before(function(done) {
    // Register new printer
    printer.register(function(err,key,id){
      if(err) throw new Error(err);
      printerID = id;
      printerKey = key;
      
      // Register new user
      user.register(function(err,newUserKey) {
        if(err) throw new Error(err);
        userKey = newUserKey;
        
        // Connect to namespace /printers
        user.connectTo("/printers",userKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          printersNSP = nsp;
          
          // Add printer to preferred printers of user
          printersNSP.emit("add",{id:printerID},function(err,responseData) {
            setTimeout(done,200);
          });
        });
      });
    });
  });
  afterEach(function() {
    if(printersNSP) printersNSP.removeAllListeners();
    if(printersRootNSP) printersRootNSP.removeAllListeners();
  });
  
  describe("when a printer connects to the cloud",function() {
    it("should trigger a changed event", function (done) {
      // reconnect user to /printers namespace
      printersNSP.once("changed",function(data) {
        debug("printer changed: ",data);
        data.online.should.equal(true);
        done();
      });
      printersNSP.once("error",function(err) {
        done(err.description);
      });
      printer.connectTo("/",printerKey,{forceNew:true},function(err,nsp) {
        if(err) throw new Error(err);
        printersRootNSP = nsp;
      });
    });
  });
  describe("when a printer disconnects from the cloud",function() {
    //this.timeout(5000);
    it("should trigger a changed event", function (done) {
      // reconnect user to /printers namespace
      printersNSP.once("changed",function(data) {
        debug("printer changed: ",data);
        data.online.should.equal(false);
        done();
      });
      printersNSP.once("error",function(err) {
        done(err.description);
      });
      debug("disconnect printer");
      // can't disconnect right away after a connect for some reason...
      setTimeout(function() {
        printersRootNSP.disconnect();
      },500);
    });
  });
});
