require("should");
require("mocha");
var er = require("../lib/ws/ioErrors");
var debug = require('debug')('cloud:test:clustering');
var config = require('../lib/config');
var fs = require('fs');
var ss = require("socket.io-stream");
var PORT = process.env.PORT ? process.env.PORT : config.PORT;
var PORT_B = process.env.PORT_B ? process.env.PORT_B : 5001;

describe('clustering', function () {
  var CLOUD_A_URL = "http://localhost:"+PORT;
  var CLOUD_B_URL = "http://localhost:"+PORT_B;
  var printer = require("./mock/printer")(CLOUD_A_URL);
  var user = require("./mock/user")(CLOUD_B_URL);
  var user2 = require("./mock/user")(CLOUD_B_URL);
  var userKey;
  var printerID;
  var printerKey;
  var userPrinterNSP;
  var printerRootNSP;
  var printerPrinterNSP;
  var userWebcamNSP;
  var user2WebcamNSP;
  var printerWebcamNSP;

  before(function(done) {
    // Register new printer
    printer.register(function(err,key,id){
      if(err) throw new Error(err);
      printerID = id;
      printerKey = key;

      // Register new user
      user.register(function(err,newUserKey) {
        if(err) throw new Error(err);
        userKey = newUserKey;

        // connect printer to / (so that namespaces are created)
        printer.connectTo("/",printerKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          printerRootNSP = nsp;

          // connect user to /{prid}-printer
          var nspName = "/"+printerID+"-printer";
          // var nspName = "/hello";
          debug("nspName: ",nspName);
          user.connectTo(nspName,userKey,{forceNew:true},function(err,nsp) {
            if(err) throw new Error(err);
            userPrinterNSP = nsp;

            // connect printer to /{prid}-printer
            printer.connectTo(nspName,printerKey,{forceNew:true},function(err,nsp) {
              if(err) throw new Error(err);
              printerPrinterNSP = nsp;

              done();
            });
          });
        });
      });
    });
  });

  describe("Events from printer to client",function() {
    var stateValue = "printing";

    it('should be forwarded in /{prid}-printer namespace', function (done) {
      userPrinterNSP.once("state",function(data) {
        debug("usersPrinterNSP: onState data:",data);
        data.should.have.keys(["state"]);
        data.state.should.equal(stateValue);
        done();
      });
      printerPrinterNSP.emit("state", {state: stateValue});
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
    });

    it('should inform new socket clients about current state from cache', function (done) {

      var newStateValue = "idle";
      printerPrinterNSP.emit("state", {state: newStateValue});
       // Register new user
      user.register(function(err,newUserKey) {
        if(err) throw new Error(err);
        var userKey = newUserKey;

        // connect user to /{prid}-printer
        var nspName = "/"+printerID+"-printer";
        user.connectTo(nspName,userKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          var userPrinterNSP = nsp;
          userPrinterNSP.once("state",function(data) {
            debug("usersPrinterNSP: onState data:",data);
            data.should.have.keys(["state"]);
            data.state.should.equal(newStateValue);
            done();
          });
          userPrinterNSP.on("error",function(err) {
            done(err.description);
          });
        });
      });
    });
    
    describe("multiple printers connections",function() {
      var printerPrinterNSP2;
      before(function(done) {
        // connect as printer again
        printer.connectTo("/"+printerID+"-printer",printerKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          printerPrinterNSP2 = nsp;
          done();
        });
      });
      it('should not send event from first connected printer back to printer', function (done) {
        printerPrinterNSP.once("stop",function() {
          done(new Error("printer should not recieve it's own event"));
        });
        printerPrinterNSP2.once("stop",function() {
          done(new Error("new printer should not recieve events from older printer connection"));
        });
        printerPrinterNSP.emit("stop");
        setTimeout(done,200);
      });
      it('should not send event from last connected printer back to printer', function (done) {
        printerPrinterNSP.once("stop",printerStopHandler1); 
        function printerStopHandler1() {
          debug("printer1 stopped");
          done(new Error("first printer connection should not recieve events from new printer connection"));
        }
        printerPrinterNSP2.once("stop",printerStopHandler2);
        function printerStopHandler2() {
          debug("printer2 stopped");
          done(new Error("printer should not recieve it's own event"));
        }
        printerPrinterNSP2.emit("stop");
        setTimeout(done,200);
      });
      
      after(function() {
        //printerPrinterNSP2.removeEventListeners();
        printerPrinterNSP2.removeAllListeners();
        printerPrinterNSP = printerPrinterNSP2;
      });
    });
    
    afterEach(function() {
      if(userPrinterNSP) userPrinterNSP.removeAllListeners();
      if(printerRootNSP) printerRootNSP.removeAllListeners();
      if(printerPrinterNSP) printerPrinterNSP.removeAllListeners();
    });
  });
  describe("Events from clients to printer",function() {
    it('should be forwarded in /{prid}-printer namespace (without arguments)', function (done) {
      printerPrinterNSP.once("stop",function() {
        debug("stop: ",arguments);
        done();
      });
      userPrinterNSP.emit("stop");
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
    });

    it('should be forwarded in /{prid}-printer namespace with data', function (done) {
      var printUrl = "https://www.youmagine.com/documents/8826/download";
      printerPrinterNSP.once("print",function(data) {
        //debug("print: ",arguments);
        data.should.have.properties(["url"]);
        data.url.should.equal(printUrl);
        done();
      });
      printerPrinterNSP.on("error",function(err) {
        done(err.description);
      });
      userPrinterNSP.emit("print",{url:printUrl});
    });

    it('should be forwarded in /{prid}-printer namespace with data and callback', function (done) {
      var printUrl = "https://www.youmagine.com/documents/8826/download";
      var errMessage = "Missing some argument";
      printerPrinterNSP.once("print",function(data,callback) {
        //debug("print: ",arguments);
        data.should.have.properties(["url"]);
        data.url.should.equal(printUrl);
        callback({err:new er.InvalidArgumentsError(errMessage)});
      });
      printerPrinterNSP.on("error",function(err) {
        done(err.description);
      });
      userPrinterNSP.emit("print",{url:printUrl},function(response) {
        response.should.have.keys(["err"]);
        response.err.should.have.keys(["name","message"]);
        response.err.name.should.equal("InvalidArgumentsError");
        response.err.message.should.equal(errMessage);
        done();
      });
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
    });
    
    it('should be forwarded in /{prid}-printer namespace with callback only', function (done) {
      var errMessage = "Missing url argument";
      printerPrinterNSP.once("stop",function(callback) {
        debug("stop: ",arguments);
        callback({err:new er.InvalidArgumentsError(errMessage)});
      });
      printerPrinterNSP.on("error",function(err) {
        done(err.description);
      });
      userPrinterNSP.emit("stop",function(response) {
        response.should.have.keys(["err"]);
        response.err.should.have.keys(["name","message"]);
        response.err.name.should.equal("InvalidArgumentsError");
        response.err.message.should.equal(errMessage);
        done();
      });
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
    });

    describe("multiple printers connections",function() {
      var printerPrinterNSP2;
      before(function(done) {
        // connect as printer again
        printer.connectTo("/"+printerID+"-printer",printerKey,{forceNew:true},function(err,nsp) {
          if(err) throw new Error(err);
          printerPrinterNSP2 = nsp;
          done();
        });
      });
      it('should send event to last connected printer when user sends event', function (done) {
        printerPrinterNSP.once("stop",function() {
          done(new Error("older printer connection should not recieve event"));
        });
        printerPrinterNSP2.once("stop",function() {
          setTimeout(done,100);
        });
        userPrinterNSP.emit("stop"); 
        userPrinterNSP.on("error",function(err) {
          done(err.description);
        });
      });
      after(function() {
        printerPrinterNSP = printerPrinterNSP2;
      });
    });
    afterEach(function() {
      if(userPrinterNSP) userPrinterNSP.removeAllListeners();
      if(printerRootNSP) printerRootNSP.removeAllListeners();
      if(printerPrinterNSP) printerPrinterNSP.removeAllListeners();
    });
  });
  describe("Streaming events from client to printer",function() {
    var fileName = "stanford_bunny_309_faces.stl";
    var filePath = "test/tmp/"+fileName;
    before(function(done) {
      fs.mkdir("test/tmp",done);
    });

    it('should be forwarded in /{prid}-printer namespace (without arguments)', function (done) {
      ss(printerPrinterNSP).once("stop",function(stream) {
        //debug("print: ");
        stream.pipe(fs.createWriteStream(filePath));
        stream.on('end',function() {
          fs.exists(filePath,function(exists) {
            if(exists) done();
            else done(new Error("file not found"));
          });
        });
      });
      var stream = ss.createStream();
      ss(userPrinterNSP).emit('stop', stream);
      fs.createReadStream("test/assets/"+fileName).pipe(stream);
      
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
    });
    
    it('should be forwarded in /{prid}-printer namespace with data', function (done) {
      ss(printerPrinterNSP).once("print",function(stream,data) {
        //debug("print: ",data);
        data.should.have.properties("contentType");
        data.contentType.should.equal("stl");
        stream.pipe(fs.createWriteStream(filePath));
        stream.on('end',function() {
          fs.exists(filePath,function(exists) {
            if(exists) done();
            else done(new Error("file not found"));
          });
        });
      });
      
      var stream = ss.createStream();
      ss(userPrinterNSP).emit('print', stream, {contentType:"stl"});
      fs.createReadStream("test/assets/"+fileName).pipe(stream);
      
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
    });
    
    it('should be forwarded in /{prid}-printer namespace with data and callback', function (done) {
      ss(printerPrinterNSP).once("print",function(stream,data,callback) {
        //debug("print: ",data,callback);
        data.should.have.properties("contentType");
        data.contentType.should.equal("stl");
        stream.pipe(fs.createWriteStream(filePath));
        stream.on('end',function() {
          fs.exists(filePath,function(exists) {
            if(exists) callback(); //done();
            else done(new Error("file not found"));
          });
        });
      });
      
      var stream = ss.createStream();
      ss(userPrinterNSP).emit('print', stream, {contentType:"stl"}, function() {
        done();
      });
      fs.createReadStream("test/assets/"+fileName).pipe(stream);
      
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
    });
    
    it('should be forwarded in /{prid}-printer namespace with callback only', function (done) {
      ss(printerPrinterNSP).once("print",function(stream,callback) {
        //debug("print: ",callback);
        stream.pipe(fs.createWriteStream(filePath));
        stream.on('end',function() {
          fs.exists(filePath,function(exists) {
            if(exists) callback(); //done();
            else done(new Error("file not found"));
          });
        });
      });
      
      var stream = ss.createStream();
      ss(userPrinterNSP).emit('print', stream, function() {
        done();
      });
      fs.createReadStream("test/assets/"+fileName).pipe(stream);
      
      userPrinterNSP.on("error",function(err) {
        done(err.description);
      });
    });
    afterEach(function() {
      ss(printerPrinterNSP).removeAllListeners();
    });
    after(function(done) {
      fs.unlink(filePath,function(err) {
        fs.rmdir("test/tmp",done);
      });
    });
  });
  describe("Streaming events (webcam) from printer to clients",function() {
    //this.timeout(100000);
    var fileName = "image0.jpg";
    var filePath = "test/tmp/"+fileName;
    var filePath2 = "test/tmp/2"+fileName;
    before(function(done) {
      // connect user to /{prid}-webcam
      var nspName = "/"+printerID+"-webcam";
      // var nspName = "/hello";
      debug("nspName: ",nspName);
      user.connectTo(nspName,userKey,{forceNew:true},function(err,nsp) {
        if(err) throw new Error(err);
        userWebcamNSP = nsp;
        // Register user 2
        user2.register(function(err,newUserKey) {
          if(err) throw new Error(err);
          var user2Key = newUserKey;
          // connect user2 to /{prid}-webcam
          user2.connectTo(nspName,user2Key,{forceNew:true},function(err,nsp) {
            debug("user2 connected ",err);
            if(err) throw new Error(err);
            user2WebcamNSP = nsp;
            // connect printer to /{prid}-printer
            printer.connectTo(nspName,printerKey,{forceNew:true},function(err,nsp) {
              debug("printer connected ",err);
              if(err) throw new Error(err);
              printerWebcamNSP = nsp;
              done();
            });
          });
        });
      });
    });
    beforeEach(function(done) {
      // create tmp files folder
      fs.mkdir("test/tmp",done);
    });

    it('should be forwarded in /{prid}-webcam namespace (without arguments)', function (done) {
      //debug("printerWebcamNSP: ",printerWebcamNSP);
      //debug("ss(printerWebcamNSP): ",ss(printerWebcamNSP));
      ss(userWebcamNSP).once("snapshot",function(stream) {
        //debug("snapshot");
        stream.pipe(fs.createWriteStream(filePath));
        stream.on('end',function() {
          fs.exists(filePath,function(exists) {
            if(exists) done();
            else done(new Error("file not found"));
          });
        });
      });
      var stream = ss.createStream();
      ss(printerWebcamNSP).emit('snapshot', stream);
      fs.createReadStream("test/assets/"+fileName).pipe(stream);
      userWebcamNSP.on("error",function(err) {
        done(err.description);
      });
      ss(printerWebcamNSP).on("error",function(err) {
        done(err.description);
      });
    });
    it('should be forwarded in /{prid}-webcam namespace to multiple clients (without arguments)', function (done) {
      //debug("printerWebcamNSP: ",printerWebcamNSP);
      //debug("ss(printerWebcamNSP): ",ss(printerWebcamNSP));
      var numDone = 2;
      var numIndex = 0;
      function checkDone() {
        numIndex++;
        if(numIndex >= numDone) done();
      }
      ss(userWebcamNSP).once("snapshot",function(stream) {
        stream.pipe(fs.createWriteStream(filePath));
        stream.on('end',function() {
          fs.exists(filePath,function(exists) {
            if(exists) checkDone();
            else done(new Error("file not found"));
          });
        });
      });
      ss(user2WebcamNSP).once("snapshot",function(stream) {
        stream.pipe(fs.createWriteStream(filePath2));
        stream.on('end',function() {
          fs.exists(filePath2,function(exists) {
            if(exists) checkDone();
            else done(new Error("file not found"));
          });
        });
      });
      
      var stream = ss.createStream();
      ss(printerWebcamNSP).emit('snapshot', stream);
      fs.createReadStream("test/assets/"+fileName).pipe(stream);
      userWebcamNSP.on("error",function(err) {
        done(err.description);
      });
      ss(printerWebcamNSP).on("error",function(err) {
        done(err.description);
      });
    });
  
    it('should be forwarded in /{prid}-webcam namespace to multiple clients (with some not listening) (without arguments)', function (done) {
      ss(userWebcamNSP).once("snapshot",function(stream) {
        stream.pipe(fs.createWriteStream(filePath));
        stream.on('end',function() {
          fs.exists(filePath,function(exists) {
            if(exists) done();
            else done(new Error("file not found"));
          });
        });
      });
      
      var stream = ss.createStream();
      ss(printerWebcamNSP).emit('snapshot', stream);
      fs.createReadStream("test/assets/"+fileName).pipe(stream);
      userWebcamNSP.on("error",function(err) {
        done(err.description);
      });
      ss(printerWebcamNSP).on("error",function(err) {
        done(err.description);
      });
    });
    
    it('should be forwarded in /{prid}-webcam namespace with data', function (done) {
      ss(userWebcamNSP).once("snapshot",function(stream,data) {
        //debug("snapshot: ",data);
        data.should.containEql({foo:"bar"});
        stream.pipe(fs.createWriteStream(filePath));
        stream.on('end',function() {
          fs.exists(filePath,function(exists) {
            if(exists) done();
            else done(new Error("file not found"));
          });
        });
      });
      var stream = ss.createStream();
      ss(printerWebcamNSP).emit('snapshot', stream, {foo:"bar"});
      fs.createReadStream("test/assets/"+fileName).pipe(stream);
      userWebcamNSP.on("error",function(err) {
        done(err.description);
      });
      ss(printerWebcamNSP).on("error",function(err) {
        done(err.description);
      });
    });
    
    it('should be forwarded in /{prid}-webcam namespace with data, ignoring callback', function (done) {
      ss(userWebcamNSP).once("snapshot",function(stream,data,callback) {
        debug("snapshot: ",data,callback);
        data.should.containEql({foo:"bar"});
        (callback === undefined).should.equal(true);
        done();
      });
      var stream = ss.createStream();
      debug('emit snapshot');
      ss(printerWebcamNSP).emit('snapshot', stream, {foo:"bar"},function(){});
      userWebcamNSP.on("error",function(err) {
        done(err.description);
      });
      ss(printerWebcamNSP).on("error",function(err) {
        done(err.description);
      });
    });

    it('should be forwarded in /{prid}-webcam namespace ignoring callback', function (done) {
      ss(userWebcamNSP).once("snapshot",function(stream,callback) {
        debug("snapshot: ",callback);
        (callback === undefined).should.equal(true);
        done();
      });
      var stream = ss.createStream();
      debug('emit snapshot');
      ss(printerWebcamNSP).emit('snapshot', stream,function(){});
      userWebcamNSP.on("error",function(err) {
        done(err.description);
      });
      ss(printerWebcamNSP).on("error",function(err) {
        done(err.description);
      });
    });
    afterEach(function(done) {
      ss(printerWebcamNSP).removeAllListeners();
      fs.unlink(filePath,function(err) {
        fs.unlink(filePath2,function(err) {
          fs.rmdir("test/tmp",done);
        });
      });
    });
  });
  after(function(done) {
    if(userPrinterNSP) userPrinterNSP.disconnect();
    if(printerRootNSP) printerRootNSP.disconnect();
    if(printerPrinterNSP) printerPrinterNSP.disconnect();
    if(userWebcamNSP) userWebcamNSP.disconnect();
    if(printerWebcamNSP) printerWebcamNSP.disconnect();
    setTimeout(done,100);
  });
});
