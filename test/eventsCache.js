require("should");
require("mocha");
var eventsCache = require("../lib/data/eventsCache.js")();
var debug = require('debug')('cloud:test:eventsCache');


describe('eventsCache', function () {


  describe('storing',function() {
    var cache;

    before(function() {
      cache = eventsCache.createCache("a");
    });
    it("should store strings",function(done) {
      var key = "mykey";
      var value = "myvalue";
      cache.set(key,value);

      cache.get(key,function(err,res){
        debug("get response: ",err,res);
        if(err !== null) return done(err);
        if(res === undefined) return done("res should not be undefined");
        else if(res === null) return done("res should not be null");
        res.should.be.an.String;
        res.should.equal(value);
        done();
      });
    });
    it("should store json",function(done) {
      var key = "myjsonkey";
      var value = {my:"value"};
      cache.set(key,value);

      cache.get(key,function(err,res){
        debug("get response: ",err,res);
        if(err !== null) return done(err);
        if(res === undefined) return done("res should not be undefined");
        else if(res === null) return done("res should not be null");
        res.should.be.an.Object;
        res.should.have.key("my");
        res.my.should.equal("value");
        done();
      });
    });
    after(function() {
      cache.clear();
    });
  });
  describe('retrieval',function() {
    var cache;

    before(function() {
      cache = eventsCache.createCache("b");
    });
    it("should be empty on init",function(done) {
      //var key = "mykey";
      //var value = "myvalue";
      cache.getAll(function(err,res){
        debug("getAll response: ",err,res);
        if(err !== null) return done(err);
        if(res === undefined) return done("res should not be undefined");
        else if(res === null) return done();
      });
    });
    it("should contain stored data",function(done){
      cache.set("name",'redis');
      cache.set("age",3);
      cache.getAll(function(err,res){
        debug("getAll response: ",err,res);
        if(err !== null) return done(err);
        if(res === undefined) return done("res should not be undefined");
        else if(res === null) return done("res should not be null");
        res.should.have.keys("name","age");
        res.name.should.equal("redis");
        res.age.should.equal(3);
        done();
      });
    });
    after(function() {
      cache.clear();
    });
  });
  describe('clearing',function() {
    var cache;

    before(function() {
      cache = eventsCache.createCache("c");
    });
    it("should be cleared",function(done) {
      cache.set("name",'redis');
      cache.set("age",'3');
      cache.clear();
      cache.getAll(function(err,res){
        debug("getAll response: ",err,res);
        if(err !== null) return done(err);
        else if(res === null) return done();
      });
    });
    after(function() {
      cache.clear();
    });
  });
  describe('using clustered',function() {
    var cacheA;
    var cacheB;

    before(function() {
      cacheA = eventsCache.createCache("sharedname");
      cacheB = eventsCache.createCache("sharedname");
    });
    it("should share data",function(done) {
      var key = "mykey";
      var value = "myvalue";
      cacheA.set(key,value);
      cacheB.get(key,function(err,res){
        debug("get response: ",err,res);
        if(err !== null) return done(err);
        if(res === undefined) return done("res should not be undefined");
        else if(res === null) return done("res should not be null");
        res.should.be.an.String;
        res.should.equal(value);
        done();
      });
    });
    after(function() {
      cacheA.clear();
      cacheB.clear();
    });
  });
});
