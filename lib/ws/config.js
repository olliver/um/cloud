'use strict';
var er            = require("./ioErrors");
var config        = require('../config');
var debug         = require('debug')('cloud:ws:config');
var userConfig    = require("../config/userConfig")();

module.exports = function(io) {
var PATH = "/config";
  
  var nsp = io.of(PATH);
  nsp.on('connection', function(socket){
    debug(PATH+": new connection: ",socket.id,"type:",socket.handshake.query.type);
        
    socket.on('getSlicers', function(data,callback){
      debug(PATH+": getSlicers");
      if(typeof callback !== 'function') callback = null;
      if(typeof data === 'function') callback = data;
      
      var slicers = userConfig.getSlicers();
      if(callback) callback(null,slicers);
    });
  });
};
