var userConfig = require("../config/userConfig")();
var debug = require('debug')('cloud:ws:ForwardingOverrides');
var er = require("./ioErrors");
var schema = require('validate');
var printerPrintSchema = schema({
  contentType: {
    type: 'string',
    message: {subject: 'contentType',
              name: 'invalid'
             }
  },
  printerType: {
    type: 'string',
    message: {subject: 'printerType',
              name: 'invalid'
             }
  },
  material: {
    type: 'string',
    message: {subject: 'material',
              name: 'invalid'
             }
  },
  profile: {
    type: 'string',
    message: {subject: 'profile',
              name: 'invalid'
             }
  },
  settings: {
    type: 'object',
    message: {subject: 'settings',
              name: 'invalid'
             }
  },
  url: {
    type: 'string',
    message: {subject: 'url',
              name: 'invalid'
             }
  }
},{strip:false});
var overrides = {
  printer: {
    print:function(eventType,stream,data,callback) {
      debug("overriding printer:print: ",eventType,data);
      
      var err;
      if(data === undefined) {
        err = new er.InvalidArgumentsError("data is required");
      } else {
        var issues = printerPrintSchema.validate(data);
        if(issues.length > 0) {
          err = new er.InvalidArgumentsError("Invalid arguments",issues);
        } else if(stream === undefined && data.url === undefined) {
          err = new er.InvalidArgumentsError("stream or url is required");
        }
      }
      if(err) {
        if(callback) callback(err);
        return;
      }
      
      if(data.contentType === undefined) {
        data.contentType = 'stl';
      }
      var overrides = { printerType: 'ultimaker2' };
      var overrideKeys = ['printerType','material','profile'];
      for(var index in overrideKeys) {
        var key = overrideKeys[index];
        if(data[key] === undefined) continue;
        overrides[key] = data[key];
      }
      //debug("  overrides: ",overrides);
      
      userConfig.getSlicerConfigEncoded(data.contentType,overrides,data.settings,function(err,config) {
       // debug("getSlicerConfigEncoded response: ",err);
        if(err) {
          if(callback) callback(new er.InvalidArgumentsError(err));
          return;
        }
       
       data.settings = config;
       //debug("  data: ",data);
       callback(err,eventType,stream,data);
      });
    }
  }
};

module.exports = function(featureName,eventType,stream,data,callback) {
  var featureOverrides = overrides[featureName];
  var overriden = false;
  if(featureOverrides !== undefined) {
    var eventOverride = featureOverrides[eventType];
    if(eventOverride !== undefined && typeof eventOverride === 'function') {
      eventOverride(eventType,stream,data,callback);
      overriden = true;
    }
  }
  if(!overriden) callback(null,eventType,stream,data);
};