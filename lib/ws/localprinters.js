'use strict';
var er = require("./ioErrors");
var PrintersData = require("../data/printersData.js");
var debug = require('debug')('cloud:ws:localprinters');

module.exports = function(io,printersData) {
  
  var PATH = "/localprinters";
  
  var nsp = io.of(PATH);
  nsp.on('connection', function(socket){
    var query = socket.handshake.query;
    if(query.type === "printer") return;
    var remoteAddress = socket.handshake.headers['x-forwarded-for'];
    if(remoteAddress === undefined) { 
      remoteAddress = socket.client.conn.remoteAddress;
    }
    debug(PATH+": new connection: ",socket.id,query.type,remoteAddress);
    if(remoteAddress === undefined) {
      throw new Error("No remoteAddress available");
    }
    
    getLocalPrinters(function(err,data) {
      if(err) throw new Error(err);
      socket.emit("list",{printers: data});
      data.forEach(function(printer) {
        socket.emit("appeared",printer);
      });
    });
    
    printersData.on(PrintersData.PRINTER_ONLINE,onPrinterOnline);
    function onPrinterOnline(printerData,printerRemoteIP) {
      //debug("PrintersData.PRINTER_ONLINE: ",printerData,printerRemoteIP);
      //debug("  remoteAddress: ",remoteAddress);
      // make sure only the clients behind the same
      // remote ip receive this
      if(printerRemoteIP !== remoteAddress) return;
      debug("checked");
      socket.emit("appeared",printerData);
      getLocalPrinters(function(err,data) {
        if(err) throw new Error(err);
        debug("emit list");
        socket.emit("list",{printers: data});
      });
    }
    printersData.on(PrintersData.PRINTER_OFFLINE,onPrinterOffline);
    function onPrinterOffline(printerData,printerRemoteIP) {
      //debug("PrintersData.PRINTER_OFFLINE: ",printerData,printerRemoteIP);
      // make sure only the clients behind the same
      // remote ip receive this
      if(printerRemoteIP !== remoteAddress) return;
      socket.emit("disappeared",printerData);
      getLocalPrinters(function(err,data) {
        if(err) throw new Error(err);
        debug("emit list");
        socket.emit("list",{printers: data});
      });
    }
    socket.on('disconnect', function(){
      debug(PATH+": disconnect (socket): ",socket.id,query.type);
      printersData.removeListener(PrintersData.PRINTER_ONLINE,onPrinterOnline);
      printersData.removeListener(PrintersData.PRINTER_OFFLINE,onPrinterOffline);
    });
    
    function getLocalPrinters(callback) {
      debug("getLocalPrinters");
      printersData.get({online:true,remoteIP:remoteAddress},
                       printersData.publicFields, 
                       function(err, data) {
        if(err) callback(err,null);
        var publicData = [];
        data.forEach(function(printer) {
          // ToDo: use user's printer name
          // ToDo: Add prefered boolean 
          publicData.push(printer.toObject());
        });
        debug("  LocalPrinters publicData: ",publicData);
        callback(null,publicData);
      });
    }
  });
};
