'use strict';
var er = require("./ioErrors");
var PrintersData = require("../data/printersData.js");
var debug = require('debug')('cloud:ws:printers');
var schema = require('validate');
var paramSchema = schema({
  id: {
    type: 'string',
    required: true,
    message: {subject: 'id',
              name: 'invalid',
              message: "id is required"
             }
  }
});

module.exports = function(io,usersData,printersData) {
  
  var PATH = "/printers";
  
  var nsp = io.of(PATH);
  nsp.on('connection', function(socket){
    //debug(PATH+": new connection: ",socket.id,"type:",socket.handshake.query.type);
    
    // ToDo: find more general place to load userdata
    // store this in socket (session)
    // make populating printers optional
    // easy reload method
    var query = socket.handshake.query;
    
    if(query.type === "printer") return;
    getPrintersList(query.key, function(err,preferredPrinters) {
      emitPrintersList(preferredPrinters,true);
      
      preferredPrinters.forEach(function(printer) {
        // listen to a printer specific update event
        printersData.on(printer.id+"-"+PrintersData.PRINTER_UPDATED,onPrinterUpdate);
      });
    });
    
    function getPrintersList(userKey,callback) {
      //debug("getPrintersList");
      usersData.getByKeyWithPrinters(userKey,function(err, userData) {
        if(err) return callback(err,null);
//        debug("getByKeyWithPrinters responded");
//        debug("  getByKeyWithPrinters: userData: ",userData);
        var preferredPrinters = [];
        userData.printers.forEach(function(printer) {
          printer = printer.toObject();
//          debug("  printer: ",printer);
          printer.id.name = printer.name;
          printer = printer.id;
//          debug("  >printer: ",printer);
          preferredPrinters.push(printer);
        });
//        debug("  publicPrintersData: ",preferredPrinters);
        callback(null,preferredPrinters);
      });
    }
    
    function emitPrintersList(preferredPrinters,emitAppeared) {
      //debug("emitPrintersList");
      preferredPrinters.forEach(function(printer) {
        if(emitAppeared) {
          //debug("emit appeared");
          socket.emit("appeared",printer);
        }
      });
      socket.emit("list",{printers: preferredPrinters});
    }
    
    socket.on('add', function(data,callback){
      debug(PATH+": add: ",data);
      if(typeof callback !== 'function') callback = null;
      if(typeof data === 'function') callback = data;
      var issues = paramSchema.validate(data);
      if(issues.length > 0) {
        var err = new er.InvalidArgumentsError("missing id argument",issues);
        if(callback) callback(err);
        return;
      }
      usersData.addPrinter(query.key,data.id,function(err,printerData) {
//        debug("usersData.addPrinter response: ",err,printerData);
        if(err) {
          if(callback) callback(new er.Error(err.message));
          return;
        }
        if(callback) callback(null,{message:"printer is added"});
        
        socket.emit("appeared",printerData);
        getPrintersList(query.key, function(err,preferredPrinters) {
          emitPrintersList(preferredPrinters,false);
        });
        // listen to a printer specific update event
        printersData.on(data.id+"-"+PrintersData.PRINTER_UPDATED,onPrinterUpdate);
      });
    });
    socket.on('remove', function(data,callback){
      debug(PATH+": remove: ",data);
      if(typeof callback !== 'function') callback = null;
      if(typeof data === 'function') callback = data;
      var issues = paramSchema.validate(data);
      if(issues.length > 0) {
        var err = new er.InvalidArgumentsError("missing id argument",issues);
        if(callback) callback(err);
        return;
      }
      usersData.removePrinter(query.key,data.id,function(err) {
//        debug("removed user: ",err);
        if(err) {
          if(callback) callback(new er.Error(err.message));
          return;
        }
        if(callback) callback(null,{message:"printer is removed"});

        socket.emit("disappeared",data);
        getPrintersList(query.key, function(err,preferredPrinters) {
          emitPrintersList(preferredPrinters,false);
        });
        printersData.removeListener(data.id+"-"+PrintersData.PRINTER_UPDATED,onPrinterUpdate);
      });
    });
    function onPrinterUpdate(printerData,changes) {
      debug("PrintersData.PRINTER_UPDATED");
      debug("  emit changed: ",printerData);
      socket.emit("changed",printerData);
      getPrintersList(query.key, function(err,preferredPrinters) {
        emitPrintersList(preferredPrinters,false);
      });
    }
    socket.on('disconnect', function(){
      //debug(PATH+": disconnect (socket): ",socket.id,"type:",query.type);
      //printersData.removeListener(PrintersData.PRINTER_UPDATED,onPrinterUpdate);
      if(query.type === "printer") return;
      
      getPrintersList(query.key, function(err,preferredPrinters) {
        preferredPrinters.forEach(function(printer) {
          // stop listening to printer specific update events
          printersData.removeListener(printer.id+"-"+PrintersData.PRINTER_UPDATED,onPrinterUpdate);
        });
      });
    });
  });
};
