'use strict';
var er = require("./ioErrors");
var debug = require('debug')('cloud:ws:authorization');

// Authorize Socket.IO Connections
module.exports = function(io,usersData,printersData) {
  
  // Add as Socket.io middleware
  io.use(authorization);

  function authorization(socket, next) {
    // ToDo: skip when this socket was already authorized
    // ToDo: breadcast authorization errors in specific namespace
    var query = socket.handshake.query;
    if (query.key === undefined) {
      return next(new er.InvalidArgumentsError("Missing key"));
    }
    // check existence
    switch(query.type) {
      case "printer":
        //debug("authorize printer: "+socket.id);
        printersData.getByKey(query.key,function(err,printer){
          if(err) {
            next(new er.Error("Error getting printer info: "+err));
          } else if (printer === null) {
            next(new er.Error("Printer unknown"));
          } else {
            next();
          }
        });
        break;
      default:
        debug("authorize user with key: "+query.key+' ('+socket.id+')');
        usersData.getByKey(query.key,function(err,user){
          if(err) {
            next(new er.Error("Error getting user info: "+err));
          } else if (user === null) {
            next(new er.Error("User unknown"));
          } else {
            next();
          }
        });
        break;
    }
  }
};
