'use strict';

var util = require("util");
var debug = require('debug')('cloud:ws:ioErrors');

/**
 * default socket.io acknowledgment callback response Error
 * @param {Object} [message] - more specific message
 * @param {Object} [issues] - array of multiple underlying issues (like validation issues)
 */
var IOError = module.exports.Error = function(message,issues) {
  debug("new IOError: ",message,issues);
  this.name = "Error";
  if(message) {
    // When for example error instances are given 
    // we need to transform it into a string
    this.message = message.toString();
  }
  if(issues) {
    this.issues = issues;
  }
};

module.exports.InvalidArgumentsError = function() {
  IOError.apply(this,arguments);
  this.name = "InvalidArgumentsError";
};
util.inherits(module.exports.InvalidArgumentsError, IOError);

module.exports.AccessDeniedError = function() {
  IOError.apply(this,arguments);
  this.name = "AccessDeniedError";
};
util.inherits(module.exports.AccessDeniedError, IOError);
