'use strict';
var debug = require('debug')('cloud:ws:root');

module.exports = function(io,usersData,printersData,rootDomain) {
  
  var PATH = "/";
  //var printerIDs = [];
  // Hash table with per printer a array of socket.io-client id's stored under their printer id. 
  // A printer can have multiple connections (one for each namespace) when it's connection isn't multiplexed. 
  // Example: 
  // Two printers that are connected to 3 namespaces each. 
  // {
  //   54eb3a26234cba614233ba2a: ['8DmbDfEzKzZaElwVAABi', 'Pinm2ZX_EyU3G3d5AABj', '9J6fc_zEgGvAah4cAABk'],
  //   54de6a4db676e37c03b465d1: ['KWg_fUkKnxZ5QdqMAABl', 'EN7_U_NeSBZ79bdiAABm', 'BKCiwiY-3pJbEgp_AABn']
  // }
  var printersConnections = {}; 

  
  var nsp = io.of(PATH);
  nsp.on('connection', function(socket){
    
    
    var query = socket.handshake.query;
    switch(query.type) {
      case "printer":

        printersData.getByKey(query.key,function(err, printerData) {
          debug(PATH+": printer connected: ",socket.id,"(id: "+printerData.id+")");
          
          // new printer connected?
          if(printersConnections[printerData.id] === undefined) {
            printersConnections[printerData.id] = [];
            
            //debug("new printer connection");
            debug("+num connected printers: ",Object.keys(printersConnections).length);
            
            var remoteAddress = socket.handshake.headers['x-forwarded-for'];
            if(remoteAddress === undefined) { 
              remoteAddress = socket.client.conn.remoteAddress;
            }
            // store printer as online
            printersData.update(printerData.id,{online:true,remoteIP:remoteAddress},function(err,doc) {
              if(err) throw new Error("Couldn't store online state: "+err);
            });
          }
          // new printer socket connection connected?
          var printerSocketIds = printersConnections[printerData.id];
          if(printerSocketIds.indexOf(socket.id) === -1) {
            printerSocketIds.push(socket.id);
          }
          debug("+printersConnections: ",printersConnections);
          
          socket.on('disconnect', function(){
            debug(PATH+": printer disconnected: ",socket.id,"(id: "+printerData.id+")");
            
            // remove connection
            var connectionIndex = printerSocketIds.indexOf(socket.id);
            if(connectionIndex !== -1) {
              printerSocketIds.splice(connectionIndex,1);
            }
            // no more connections to printer? printer is offline.
            if(printerSocketIds.length === 0) {
              delete printersConnections[printerData.id];
              
              // store printer as offline
              printersData.update(printerData.id,{online:false},function(err,doc) {
                if(err) throw new Error("Couldn't store online state: "+err);
              });
              debug("-num connected printers: ",Object.keys(printersConnections).length);
            }
            
            debug("-printersConnections: ",printersConnections);
          });
        });
        break;
      default: 
        var userID;
        usersData.getByKey(query.key,function(err, userData) {
          userID = userData.id;
          debug(PATH+": user connected: ",socket.id,"(id: "+userID+")");
        });
        socket.on('disconnect', function(){
          debug(PATH+": user disconnected: ",socket.id,"(id: "+userID+")");
        });
        break;
    }
  });
  process.on('SIGINT', function() {
    debug("SIGINT");
    gracefullShutdown();
  });
  process.on('SIGTERM', function() {
    debug("SIGTERM");
    gracefullShutdown();
  });
  rootDomain.on('error', function(err) {
    debug("error: ",err);
    gracefullShutdown(err);
  });
  function gracefullShutdown(err) {
    debug("gracefullShutdown err: ",err);
    var printerIDs = Object.keys(printersConnections);
    if(printerIDs.length > 0) {
      printersData.update(printerIDs,{online:false},function(updateErr) {
        if(updateErr) debug("Error: Couldn't store online state: "+updateErr);
        rootDomain.exit();
        if(err) throw err;
        else process.exit(1);
      });
    } else {
      rootDomain.exit();
      if(err) throw err;
      else process.exit(1);
    }
    process.removeListener('SIGINT',gracefullShutdown);
    process.removeListener('SIGTERM',gracefullShutdown);
    rootDomain.removeListener('error', gracefullShutdown);
  }
};
