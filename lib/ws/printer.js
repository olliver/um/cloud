// per printer:
//   create namespaces
//   forward all events in all namespaces
//     from printer to all other clients
//     from all app's to printer

'use strict';
var er            = require("./ioErrors");
var PrintersData  = require("../data/printersData.js");
var config        = require('../config');
var redisPubSub   = require('redis-pubsubber')('cloud-printer',config.REDIS_PORT,config.REDIS_HOST);
var debug         = require('debug')('cloud:ws:printer');
var eventsCache   = require("../data/eventsCache.js")();
var ss            = require("socket.io-stream");
var EventEmitter  = require("events").EventEmitter;
var net           = require('net');
var overrider     = require('./overrider.js');
var streamManager = require('./StreamManager')();

redisPubSub.on('error',function(err) {
  debug("printer namespaces redis error: ",err);
});

module.exports = function(io,usersData,printersData) {
  
  // keep a record of all namespaces (per printer)
  var namespaces = {};
  
  // start streamManager (manages and shares streams between instances)
  var tcpPort = process.env.TCP_PORT ? process.env.TCP_PORT : config.TCP_PORT;
  var tcpHost = process.env.TCP_HOST ? process.env.TCP_HOST : config.TCP_HOST;
  streamManager.init(tcpPort,tcpHost);
  
  printersData.on(PrintersData.PRINTER_ONLINE,function(printerData,printerRemoteIP) {
    debug("PRINTER_ONLINE: ");
    if(namespaces[printerData.id] !== undefined) return;
    var printerNamespaces = [];
    var nsp = new ForwardingNSP(printerData.id,printerData.id,"");
    nsp.nsp.use(authorize);
    printerNamespaces.push(nsp);
    for(var i in printerData.features) {
      var featureName = printerData.features[i];
      nsp = new ForwardingNSP(printerData.id+"-"+featureName,printerData.id,featureName);
      nsp.nsp.use(authorize);
      printerNamespaces.push(nsp);
    }
    namespaces[printerData.id] = printerNamespaces;
    
    function authorize(socket, next) {
      var query = socket.handshake.query;
      if(query.type === "printer") return next();
      debug(socket.nsp.name+": authorize");
      usersData.getByKey(query.key,function(err,user) {
        if(err) return next({data: err});
        if(user === null) {
          return next({data: new er.Error("User unknown")});
        }
        // is preferred printer? 
        for(var i=0;i<user.printers.length;i++) {
          var preferredPrinter = user.printers[i];
          if(preferredPrinter.id == printerData.id) {
            return next();
          }
        }
        
        // is client behind same remoteIP as printer? 
        var remoteAddress = socket.handshake.headers['x-forwarded-for'];
        if(remoteAddress === undefined) { 
          remoteAddress = socket.client.conn.remoteAddress;
        }
        if(socket.handshake.query.forceremote) remoteAddress = "x";
        if(remoteAddress === printerRemoteIP) {
          next(); 
        } else {
          debug(socket.nsp.name+": user remote IP: ",remoteAddress);
          debug(socket.nsp.name+": printer remote IP: ",printerRemoteIP);
          next({data:new er.AccessDeniedError("Can't connect to printer that's not local and not preferred")});
        }
      });
    }
  });
  
//  printersData.on(PrintersData.PRINTER_OFFLINE,function(printerData) {
//    debug("PRINTER_OFFLINE");
//  });
  
  function ForwardingNSP(nspName,printerID,featureName) {
    debug("/"+nspName+" opening");
    var nsp = this.nsp = io.of("/"+nspName);
    var printerSocket = null;
    var streamingPrinterSocket = null;
    var serverEventsCache = {online:{online:true}}; // cache of events from server (like online)
    var printerEventsCache = eventsCache.createCache(nspName);
    // to enable clustering we use redis pub/sub
    // to talk to the printer socket
    var eventsChannel = redisPubSub.createChannel(nspName);
    var streamingChannel = redisPubSub.createChannel(nspName+"-streaming");
    var currentStreamID;
    
    nsp.on('connection', function(socket){
      var query = socket.handshake.query;
      if(query.type !== "printer") query.type = "user";
      var streamingSocket = ss(socket);
      socket.join(query.type); // group sockets in rooms per type
      var outgoingStream;
      // receive pub/sub events to printers
      function onEventsMessage(targetType) {
        //debug("received events pub/sub message: ",arguments);
        if(query.type !== targetType) return;
        var args = Array.prototype.slice.call(arguments,1);
        socket.emit.apply(socket,args);
      }
      function onStreamingMessage(targetType,serverPort,streamID) {
        if(query.type !== targetType) return;
        debug("received streaming pub/sub message: ",arguments);
        var args = Array.prototype.slice.call(arguments,3);
        
        streamManager.get(streamID,serverPort,function(err,stream) {
          if(err) {
            debug('Error retrieving stream ',streamID,' from ',serverPort,': ',err);
            return; 
          }
          
          // remove prev outgoing stream
          // prevents memory leak of unhandled streams
          if (outgoingStream !== undefined) {
            debug('remove prev outgoing stream: ',outgoingStream.id);
            outgoingStream.end();
            outgoingStream.destroy();
          }
          
          outgoingStream = ss.createStream();
          outgoingStream.on('error',function(err) {
            debug('outgoingStream error: ',outgoingStream.id,' from: ',streamID,' error: ',err);
            // manually destroy streams
            outgoingStream.end();
            outgoingStream.destroy();
            streamManager.remove(streamID);
          });
          outgoingStream.on('end',function() {
            debug('outgoingStream end: ',outgoingStream.id,' from: ',streamID);
          });
          args.splice(1,0,outgoingStream);
          
          // pipe stream over socket.io-stream to consumer
          debug('pipe stream to consumer');
          debug('  socket.id: ',socket.id);
          debug('  streamID: ',streamID);
          debug('  serverPort: ',serverPort);
          streamingSocket.emit.apply(streamingSocket,args);
          stream.pipe(outgoingStream);
          debug('  outgoingStream.id: ',outgoingStream.id);
        });
      }
      if(query.type === "printer") {
        debug("/"+nspName+": new printer: ",socket.id);
        if(printerSocket !== null) {
          printerSocket.disconnect();
        }
        printerSocket = socket;
        streamingPrinterSocket = ss(socket);
        // only when we have a reference to the printer socket
        // we listen for printer pub/sub events
      } else {
        debug("/"+nspName+": new user: ",socket.id);
        printerEventsCache.getAll(function(err,res) {
          if(err) debug("can't retrieve events cache: ",err);
          for(var eventType in res) {
            var eventData = res[eventType];
            debug("/"+nspName+": Cached event: ",eventType,eventData);
            socket.emit(eventType,eventData);
          }
        });
        // emit cached events from server (like online)
        debug("/"+nspName+": serverEventsCache: ",serverEventsCache);
        for(var eventType in serverEventsCache) {
          socket.emit(eventType,serverEventsCache[eventType]);
        }
      }
      eventsChannel.on("message", onEventsMessage);
      streamingChannel.on("message", onStreamingMessage);
      
      createAnyEvents(socket);
      createAnyEvents(ss(socket));
      socket.on('any', handleAnyEvent);
      streamingSocket.on('any', config.SOCKET_IO_STREAM_OPTS,handleAnyEvent);
      function handleAnyEvent(eventType) {
        var iArgs = Array.prototype.slice.call(arguments);
        var callback;
        var incomingStream;
        var data;
        if(typeof iArgs[iArgs.length-1] === 'function') {
          callback = iArgs.pop();
        }
        if(iArgs[1] instanceof EventEmitter) {
          incomingStream = iArgs.splice(1,1)[0];
        }
        if(iArgs.length > 1 && typeof iArgs[1] !== 'function') {
          data = iArgs[1];
        }
        debug("/"+nspName+": "+query.type+" sends "+((incomingStream)? "streaming " : "")+"event: ",eventType,data);
        
        overrider(featureName,eventType,incomingStream,data,function(err,eventType,incomingStream,data) {
          //debug("overrider err: ",err);
          if(err) {
            if(callback) callback(err); 
            return;
          } 
          var oArgs = [eventType];
          if(data) oArgs.push(data);
          if(callback && query.type === "user") {
            oArgs.push(callback);
          }
//          debug("/"+nspName+": send event: ",oArgs);
//          debug("has incomingStream: ",(incomingStream != undefined));
//          debug("has printerSocket: ",(printerSocket !== null));
          var outgoingStream;
          if(incomingStream) {
            // is there already one stream in this namespace (in this instance)? 
            // and is it streaming (being consumed)?
            if(currentStreamID) {
              var alreadyStreaming = streamManager.isStreaming(currentStreamID);
              debug('already streaming: ',alreadyStreaming);
              if(alreadyStreaming) {
                // Cancel stream: Still streaming a previous stream in this namespace
                if(callback) callback(new er.Error("Still streaming a previous stream in this namespace"));
                return;
              } else {
                streamManager.remove(currentStreamID);
              }
            }
            
            // if from user and this instance has the socket connection to the printer
            if(query.type === "user" && printerSocket !== null) {
              //debug("  send stream from client to printer directly");
              outgoingStream = ss.createStream();
              oArgs.splice(1,0,outgoingStream); 
            } else {
              //debug("  save stream (accessible through tcp server)");
              currentStreamID = nspName+'/'+incomingStream.id;
              oArgs.unshift(tcpPort,currentStreamID);
              streamManager.add(currentStreamID,incomingStream,config.STREAM_TIMEOUT);
            }
          }
          if(query.type === "printer") { // from printer
            // forward all events from printer to other clients in namespace
            if(incomingStream) {
              oArgs.unshift("user"); // specify targetType
              debug("streamingChannel:publish to users: ",eventType);
              // pub/sub to all instances that there is a stream available
              streamingChannel.publish.apply(streamingChannel,oArgs);
            } else {
              //debug("broadcast to users: ",eventType);
              //socket.broadcast.emit(eventType,data);
              debug("eventsChannel:publish to users: ",eventType);
              oArgs.unshift("user"); // specify targetType
              eventsChannel.publish.apply(eventsChannel,oArgs);
              if(data !== undefined) printerEventsCache.set(eventType,data);
            }
          } else { // from client
            // forward all events from clients to printer
            if(printerSocket === null) {
              // publish the event through pub/sub so
              // the server instance with the printer socket
              // can emit it to the printer
              if(incomingStream) { 
                oArgs.unshift("printer"); // specify targetType
                debug("streamingChannel:publish to printer: ",eventType);
                // pub/sub to all instances that there is a stream available
                streamingChannel.publish.apply(streamingChannel,oArgs);
              } else {
                debug("eventsChannel:publish to printer: ",eventType);
                oArgs.unshift("printer"); // specify targetType
                eventsChannel.publish.apply(eventsChannel,oArgs);
              }
            } else {
              if(incomingStream) {
                debug("streaming emit for printer: ",eventType);
                streamingPrinterSocket.emit.apply(streamingPrinterSocket,oArgs);
                incomingStream.pipe(outgoingStream);
              } else {
                debug("emit to printer: ",eventType);
                printerSocket.emit.apply(printerSocket,oArgs);
              }
            }
          }
        });
      }
      socket.on('disconnect', function(){
        socket.removeAllListeners();
        ss(socket).removeAllListeners();
        if(query.type === "printer") {
//          debug("/"+nspName+": printer disconnected: ",socket.id);
          printerSocket = null;
          printerEventsCache.clear();
        } else {
//          debug("/"+nspName+": user disconnected: ",socket.id);
        }
        eventsChannel.removeListener("message",onEventsMessage);
        streamingChannel.removeListener("message",onStreamingMessage);
      });
    });
    printersData.on(printerID+"-"+PrintersData.PRINTER_UPDATED,function(publicPrinterData,changes) {
      //debug("/"+nspName+" PRINTER_UPDATED: ",changes);
      if(changes.online === undefined) return;
      var eventType = "online";
      var data = {online:changes.online};
      //debug("eventsChannel:publish to users: "+eventType+": ",data);
      //eventsChannel.publish("user",eventType,data);
      //debug("/"+nspName+" broadcast to room 'user': "+eventType+": ",data);
      nsp.to('user').emit(eventType,data);
      serverEventsCache[eventType] = data;
    });
  }
  function createAnyEvents(socket) {
    var streamEvent = ss.Socket.event;
    // socket.io socket
    if(socket.onevent !== undefined) {
      var originalOnEvent = socket.onevent;
      socket.onevent = function() {
        // emit regular event
        originalOnEvent.apply(socket, arguments);
        var data = arguments[0].data;
        // ignore any and internal socket.io-stream events
        if(data[0] === 'any' || data[0].indexOf(streamEvent) === 0) return;
        // Note: turn this event into a 'any' event
        // We add the event type as first argument, the regular arguments 
        // (data and callback) become the subsequent arguments
        data.unshift('any');
        // emit 'any' event
        originalOnEvent.apply(socket, arguments);
      };
    } else if(socket.sio && socket.$emit) {
      // listen for stream events on original socket.io socket
      socket.sio.on(streamEvent,function() {
        var args = Array.prototype.slice.call(arguments);
        // Chanding original event to any event, 
        // adding original event type as argument
        // from: eventType, pos, streamID, data, callback
        // to: any, pos, eventType, streamID, data, callback
        // Adding original eventType after pos:
        args.splice(2,0,args[0]); 
        // Changing event type to any:
        args[0] = 'any'; 
        // Increment pos (streamID position) to 1 
        // (because we added eventType in front of it)
        //args[1] = [1]; 
        for(var i in args[1]) args[1][i]++;
        socket.$emit.apply(socket,args);
      });
    } else {
      debug("Error: Can't create 'any' event");
    }
  }
};
