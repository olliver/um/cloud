'use strict';

var rootDomain = require('domain').create();
rootDomain.run(init);

function init() {
  var pmx = require('pmx').init({http: false}); // Keymetrics agent
  var config = require('./config');
  var LOOK_PORT = process.env.LOOK_PORT ? process.env.LOOK_PORT : config.LOOK_PORT;
  require('look').start(LOOK_PORT); // performance profiler based on nodetime
  var express = require('express');
  var bodyParser = require('body-parser');
  //var cookieParser = require('cookie-parser');
  //var session = require('express-session');
  //var RedisStore = require('connect-redis')(session);
  var morgan  = require('morgan');
  
  var app = express();
  var http = require('http').Server(app);
  var io = require('socket.io')(http,config.SOCKET_IO_OPTS);
  var redis = require('redis');
//  var RedisAdapter = require('socket.io-redis');
  var debug = require('debug')('cloud:index');
  var debugBlocked = require('debug')('cloud:blocked');
  var blocked = require('blocked');

  var PORT = process.env.PORT ? process.env.PORT : config.PORT;

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded());
  
  //app.use(cookieParser());
  // Future: set session cookie to secure
  //app.use(session({secret: 'jiskefet',
  //                 name: 'hps.sid',
  //                 resave:true,
  //                 saveUninitialized:true,
  //                 cookie: { secure: true }}));
  app.use(morgan('dev'));
  app.disable('etag');
  debug('HTTP ETag: ',app.get('etag'));
  
  http.listen(PORT, function(){
    debug('server listening on *:' + PORT);
  });

//  io.adapter(initRedisAdapter(config.REDIS_PORT,config.REDIS_HOST));

  // DATA
  var mongoose = require('mongoose');
  var db = mongoose.connect('mongodb://localhost/cloud').connection;
  db.on('error', function(err) {
      debug("MongoDB error: ",err);
  });
  var printersData = require('./data/printersData.js')();
  var usersData = require('./data/usersData.js')(printersData);

  // REST API
  require('./rest/user.js')(app,usersData);
  require('./rest/printer.js')(app,printersData);
  // Web socket API
  require('./ws/authorization.js')(io,usersData,printersData);
  require('./ws/root.js')(io,usersData,printersData,rootDomain);
  require('./ws/printer.js')(io,usersData,printersData);
  require('./ws/printers.js')(io,usersData,printersData);
  require('./ws/localprinters.js')(io,printersData);
  require('./ws/config.js')(io);

  printersData.retrieveCurrentStatus();

  function initRedisAdapter(port,host) {
    // ToDo: simplify socket.io-redis error handling, see:
    // https://github.com/Automattic/socket.io-redis/issues/26
    var pub = redis.createClient(port,host,{detect_buffers: true});
    pub.on('error',onRedisError);
    var sub = redis.createClient(port,host,{detect_buffers: true});
    sub.on('error',onRedisError);
    sub.setMaxListeners(0);
    var redisAdapter = RedisAdapter({pubClient: pub,
                            subClient: sub,
                            key: 'socket.io-cloud'});
    redisAdapter.prototype.on('error',onRedisError);
    function onRedisError(err){
      debug("Redis error: ",err);
    }
    return redisAdapter;
  }
  
  // monitoring
  blocked(function(ms){
    debugBlocked('%sms', ms | 0);
  });
  var probe = pmx.probe();
  var loopDelayMean = probe.histogram({
    name        : 'Loop delay mean',
    measurement : 'mean'
  });
  var loopDelayMin = probe.histogram({
    name        : 'Loop delay min',
    measurement : 'min'
  });
  var loopDelayMax = probe.histogram({
    name        : 'Loop delay max',
    measurement : 'max'
  });
  var loopDelayMedian = probe.histogram({
    name        : 'Loop delay median',
    measurement : 'median'
  });
  var loopDelayP75 = probe.histogram({
    name        : 'Loop delay p75',
    measurement : 'p75'
  });
  var loopDelayP95 = probe.histogram({
    name        : 'Loop delay p95',
    measurement : 'p95'
  });
  // ToDo: measurement? mean?
  probeLoopDelay(function(ms){
    loopDelayMean.update(ms);
    loopDelayMin.update(ms);
    loopDelayMax.update(ms);
    loopDelayMedian.update(ms);
    loopDelayP75.update(ms);
    loopDelayP95.update(ms);
  });
}
function probeLoopDelay(fn) {
  var start = process.hrtime()
  var interval = 100;
  setInterval(function(){
    var delta = process.hrtime(start);
    var nanosec = delta[0] * 1e9 + delta[1];
    var ms = nanosec / 1e6;
    var n = ms - interval;
    fn(Math.round(n))
    start = process.hrtime();
  }, interval).unref();
};