'use strict';

var util = require("util");
var EventEmitter = require("events").EventEmitter;
var rand = require("generate-key");
var User = require("./userModel.js");  // Mongoose Model
var Printer = require("./userModel.js");  // Mongoose Model
var debug = require('debug')('cloud:usersData');
module.exports = UsersData;

function UsersData(printersData) {
  if (!(this instanceof UsersData)) return new UsersData(printersData);

  /**
   * Create a new user
   * @param {Function} callback - receives err and created users
   */
  this.create = function(callback) {
    var user = new User();
    user.key = user.id+rand.generateKey("20");
    debug("user created: ",user);
    user.save(callback);
  };

  /**
   * get user by it's key
   * @param {String} key
   * @param {Object} [fields] - fields to select
   * @param {Object} [options]
   * @param {Function} [callback] - callback function that receives err and found user
   */
  this.getByKey = function(key) {
    User.findOne({key:key},arguments[1],arguments[2],arguments[3]);
  };

  /**
   * get user by it's key with all the public printer information
   * @param {String} key
   * @param {Function} [callback] - callback function that receives err and found user
   */
  this.getByKeyWithPrinters = function(key,callback) {
    User.findOne({key:key},function(err,user) {
      Printer.populate(user,
                       {path:"printers.id",select:printersData.publicFields},
                       function(err,user) {
        callback(err,user);
      });
    });
  };
  this.addPrinter = function(userKey,printerID,callback) {
    debug("addPrinter: ",userKey,printerID);
    // check if printer exists
    printersData.getByID(printerID,printersData.publicFields,function(err,printerData) {
      if(err) {
        return callback(new Error("Can't verify printer existance ("+err+")"),null);
      } else if(printerData === null) {
        return callback(new Error("Printer doesn't exist"),null);
      }
      printerData = printerData.toObject();
      //debug("  printerData: ",printerData);
      
      // only add when it's not already added
      User.findOne({key:userKey,'printers.id':printerData.id},
                   function(err,userData) {
        if(userData !== null) {
          return callback(new Error("Printer was already added"),null);
        }
        // Add printer to users printers list
        // add a default name and id (objectID)
        var newPrinter = {id:printerData.id,name:printerData.name};
        User.findOneAndUpdate({key:userKey},
                              {$addToSet: {printers:newPrinter}},
                              function(err,doc) {
          debug("  added printer: ",err,doc);
          callback(err,printerData);
        });
      });
    });
  };
  this.removePrinter = function(userKey,printerID,callback) {
    debug("removePrinter: ",userKey,printerID);

    // only remove when it's a preferred printer for user
    User.findOne({key:userKey,'printers.id':printerID},
                 function(err,userData) {
      debug("user match: ",userData);
      if(userData === null) {
        return callback(new Error("Printer wasn't found as preferred printer for user"));
      }
      
      // Pull printer from user's printer list
      User.findOneAndUpdate({key:userKey},
                            {$pull: {printers:{id:printerID}}},
                            function(err,doc) {
        debug("  removed printer: ",err,doc);
        callback(err);
      });
    });
  };
}
util.inherits(UsersData, EventEmitter);
