'use strict';

var mongoose = require('mongoose');

var printerSchema = mongoose.Schema({
  key: {type: String, unique: true, index: true},
  features: Array,
  name: String,
  online: Boolean,
  remoteIP: String
});
/**
 * toObject transform filter
 * Removes _id unless include_id is added to
 * toObject function's options.
 * Removes __v unless include__v is added to options.
 * More properties can be removed by specifiging them
 * in an array in a hide option.
 */
function filter(doc, ret, options) {
  if(!options.include_id) {
    delete ret._id;
  }
  if(!options.include__v) {
    delete ret.__v;
  }
  if (options.hide) {
    options.hide.forEach(function (prop) {
      delete ret[prop];
    });
  }
  if (options.only) {
    var only = options.only.split(" ");
    var filtered = {};
    only.forEach(function(key) {
      filtered[key] = ret[key];
    });
    ret = filtered;
    return ret;
  }
}
printerSchema.set("toObject",{getters:true,transform:filter});
printerSchema.methods.toFilteredObject = function(filter) {
  return this.toObject({only:filter,transform:true,getters:true});
};
module.exports = mongoose.model("Printer", printerSchema);
