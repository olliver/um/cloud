'use strict';

var config = require('../config');
var util = require("util");
var EventEmitter = require("events").EventEmitter;
var rand = require("generate-key");
var Printer = require("./printerModel.js");  // Mongoose Model
//var redisPubSub = require('../util/redis-pubsub')('cloud-printers-update',config.REDIS_PORT,config.REDIS_HOST);
var redisPubSub = require('redis-pubsubber')('cloud-printersdata',config.REDIS_PORT,config.REDIS_HOST);
var debug = require('debug')('cloud:printersData');

module.exports = PrintersData;
PrintersData.PRINTER_UPDATED  = "printerUpdated";
PrintersData.PRINTER_ONLINE   = "printerOnline";
PrintersData.PRINTER_OFFLINE  = "printerOffline";

redisPubSub.on('error',function(err){
  debug("redis error: ",err);
});
var printerUpdateChannel = redisPubSub.createChannel("printerUpdate");
var printersUpdateChannel = redisPubSub.createChannel("printersUpdate");

function PrintersData() {
  if (!(this instanceof PrintersData)) return new PrintersData();

  var _self = this;
  printerUpdateChannel.on("message", function (publicPrinterData,changes,rawPrinterData) {
    //debug("received printer update");
    emitPrinterUpdate(publicPrinterData,changes,rawPrinterData);
  });
  printersUpdateChannel.on("message", function (publicPrintersData,changes,rawPrintersData) {
    //debug("received printers update");
    for(var i in publicPrintersData) {
      emitPrinterUpdate(publicPrintersData[i],changes,rawPrintersData[i]);
    }
  });
  function emitPrinterUpdate(publicPrinterData,changes,rawPrinterData) {
    //debug('emitPrinterUpdate : ', arguments);
    _self.emit(PrintersData.PRINTER_UPDATED,publicPrinterData,changes);
    // emit a printer specific event
    var eventName = publicPrinterData.id+"-"+PrintersData.PRINTER_UPDATED;
    //debug("emit: "+eventName);
    _self.emit(eventName,publicPrinterData,changes);
    // emit online, offline events
    if(changes.online !== undefined) {
      var remoteIP = rawPrinterData.remoteIP;
      if(changes.online === true) {
        //debug("emit PRINTER_ONLINE");
        _self.emit(PrintersData.PRINTER_ONLINE,publicPrinterData,remoteIP);
      } else {
        //debug("emit PRINTER_OFFLINE");
        _self.emit(PrintersData.PRINTER_OFFLINE,publicPrinterData,remoteIP);
      }
    }
  }
  /**
   * Public fields of printers
   * These fields can be shared with users
   * Can be used to limit mongodb queries
   */
  this.publicFields = "id name features online";

  /**
   * Retrieve current printer status from db and emit events
   * Required in case instances restart and therefore 
   * missed old printer came online events. 
   */
  this.retrieveCurrentStatus = function() {
    debug('retrieveCurrentStatus');
    this.get({online:true}, function(err, data) {
      if(err) {
        debug("[Error] can't retreive current online printers.");
        return;
      }
      debug('get online printers: ');
      data.forEach(function(printer) {
        emitPrinterUpdate(printer.toFilteredObject(_self.publicFields),{online:true},printer);
      });
    });
  }
  
  /**
   * Create a new printer
   * @param {String} name - human readable name
   * @param {Array} features - supported features (like printer, slice, webcam etc)
   * @param {Function} callback - receives err and created printer
   */
  this.create = function(name,features,callback) {
    var printer = new Printer();
    printer.name = name;
    printer.features = features;
    printer.key = printer.id+rand.generateKey("20");
    printer.online = false;
    printer.remoteIP = "";
    debug("printer created: ",printer);
    printer.save(callback);
  };

  /**
   * get printer by it's key
   * @param {String} key
   * @param {Object} [fields] - fields to select
   * @param {Object} [options]
   * @param {Function} [callback] - callback function that receives err and found printer
   */
  this.getByKey = function(key) {
    // Future: find nicer way to forward remaining arguments
    Printer.findOne({key:key},arguments[1],arguments[2],arguments[3]);
  };

  /**
   * get printer by it's id
   * @param {String} id
   * @param {Object} [fields] - fields to select
   * @param {Object} [options]
   * @param {Function} [callback] - callback function that receives err and found printer
   */
  this.getByID = function(id) {
    Printer.findOne({_id:id},arguments[1],arguments[2],arguments[3]);
    // ToDo: use: Model.findById(id, [fields], [options], [callback])
  };

  /**
   * get printers
   * @param {Object} conditions
   * @param {Object} [fields] - fields to select
   * @param {Object} [options]
   * @param {Function} [callback] - callback function that receives err and found printers
   */
  this.get = function(conditions,fields,options,callback) {
    Printer.find(conditions,fields,options,callback);
  };
  
  /**
   * get all printers
   * @param {Object} fields - fields to select
   * @param {Function} [callback] - callback function that receives err and found printers
   */
  this.getAll = function(fields,callback) {
    Printer.find({},fields,callback);
  };
  
  this.update = function(id,changes,callback) {
    debug("update: ",id,changes);
    if(id instanceof Array) {
      Printer.find({_id:{$in:id}},function(err,prevPrintersData){
        //console.log("prevPrintersData: ",err,prevPrintersData);
        if(err) return callback(err);
        Printer.update({_id:{$in:id}},changes,{ multi: true },function(err,numberAffected,rawResponse) {
          //debug("printers updated: ",err,numberAffected,rawResponse);
          if(err === null) {
            var rawPrintersData = prevPrintersData;
            var publicPrintersData = [];
            for(var i in prevPrintersData) {
              for(var change in changes) {
                rawPrintersData[i][change] = changes[change];
              }
              var printerData = rawPrintersData[i].toFilteredObject(_self.publicFields);
              publicPrintersData.push(printerData);
            }
            //debug("publish printers update");
            printersUpdateChannel.publish(publicPrintersData,changes,rawPrintersData);
          }
          callback(err,rawResponse);
        });
      });
    } else {
      Printer.findOne({_id:id},function(err,prevPrinterData){
        if(err !== null) return callback(err);
        var necessary = false;
        for(var key in changes) {
          if(prevPrinterData[key] !== changes[key]) necessary = true;
        }
        if(!necessary) return callback(null,null);
        
        Printer.findOneAndUpdate({_id:id},changes,function(err,rawPrinterData) {
          if(err !== null) return callback(err);
          var publicPrinterData = null;
          if(rawPrinterData !== null) {
            publicPrinterData = rawPrinterData.toFilteredObject(_self.publicFields);
          }
          callback(err,publicPrinterData);
          if(err === null) {
            // pub PRINTER_UPDATED
            //debug("publish printer update");
            printerUpdateChannel.publish(publicPrinterData,changes,rawPrinterData);
          }
        });
      });
    }
  };
}
util.inherits(PrintersData, EventEmitter);
