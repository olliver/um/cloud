'use strict';

var mongoose = require('mongoose');

var ObjectId = mongoose.Schema.Types.ObjectId;
// sub document schema for printer data per user
var usersPrinterSchema = mongoose.Schema({
  name:String,
  id:{type:ObjectId, ref: 'Printer'}
});
// main schema for user
var userSchema = mongoose.Schema({
  key: {type: String, unique: true, index: true},
  printers: [usersPrinterSchema]
});
/**
 * toObject transform filter
 * Removes _id unless include_id is added to
 * toObject function's options.
 * Removes __v unless include__v is added to options.
 * More properties can be removed by specifiging them
 * in an array in a hide option.
 */
function filter(doc, ret, options) {
  if(!options.include_id) {
    delete ret._id;
  }
  if(!options.include__v) {
    delete ret.__v;
  }
  if (options.hide) {
    options.hide.forEach(function (prop) {
      delete ret[prop];
    });
  }
}
userSchema.set("toObject",{getters:true,transform:filter});
module.exports = mongoose.model("User", userSchema);
