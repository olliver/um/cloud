'use strict';

var config = require('../config');
var redis = require('redis');
var debug = require('debug')('cloud:eventsCache');

module.exports = EventsCache;

function EventsCache() {
  if (!(this instanceof EventsCache)) return new EventsCache();

  var _self = this;
  var _client = redis.createClient(config.REDIS_PORT,config.REDIS_HOST);
  _client.on('error',function(err){
    debug("Redis error: ",err);
  });

  /**
   * Create a new cache container
   * Stored in Redis under one name (hashkey)
   */

  this.createCache = function(name) {
    return new Cache(name);
  };
  function Cache(name) {

    this.get = function(key,callback) {
//      debug("get: ",key);
//      debug("  calling hget: ",name,key);
      _client.hget(name, key, function(err, res) {
        if(err) return callback(err);
        res = JSON.parse(res);
        callback(null,res);
      });
    };
    this.getAll = function(callback) {
//      debug("getAll");
//      debug("  calling hgetall: ",name);
      _client.hgetall(name, function(err, res) {
        if(err) return callback(err);
        for(var key in res) {
          res[key] = JSON.parse(res[key]);
        }
        callback(null,res);
      });
    };
    this.set = function(key,value) {
//      debug("set: ",key,value);
      value = JSON.stringify(value);
//      debug("  calling hset: ",name,key,value);
      _client.hset(name, key, value);
    };
    this.clear = function() {
//      debug("clear: ",name);
      _client.del(name);
    };

  }
}
