'use strict';
var debug = require('debug')('cloud:rest:user');
var er = require("../ws/ioErrors");

module.exports = function(app,usersData) {
  app.post('/user/register', function(req, res){
    usersData.create(function(err,user) {
      // Future: limit access to our own "apps" only
      res.header('Access-Control-Allow-Origin', '*');
      if(err) {
        var err = new er.IOError("Error registering user: "+err);
        debug("Error registering user: "+err);
        return res.status(400).send({err:err});
      }
      res.status(200).send(user.toObject());
    });
  });
};
