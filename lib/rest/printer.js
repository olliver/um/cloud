'use strict';
var debug = require('debug')('cloud:rest:printer');
var er = require("../ws/ioErrors");
var schema = require('validate');
var headerSchema = schema({
});
var registerSchema = schema({
  name: {
    type: 'string',
    required: true,
    message: {subject: 'name',
              name: 'invalid',
              message: "name is required"
             }
  },
  features: {
    type: 'array',
    required: true,
    message: {subject: 'features',
              name: 'invalid',
              message: "features is required"
             }
  },
});
module.exports = function(app,printersData) {
  
  app.post('/printer/register', register);
  app.options('/printer/register', register); 
  function register(req, res) {
    res.header('Access-Control-Allow-Origin', '*');
    // body is parsed by body-parser module
    var body = req.body;
    debug("/printer/register body: ",body);
    //debug("  req.headers: ",req.headers);
    
    var headerIssues = headerSchema.validate(req.headers);
    var bodyIssues = registerSchema.validate(body);
    var issues = headerIssues.concat(bodyIssues);
    if(issues.length > 0) {
      var err = new er.InvalidArgumentsError("",issues);
      return res.status(400).send({err:err});
    }
    // ToDo what happens when MongoDB is down?
    printersData.create(body.name,body.features,function(err,printer) {
      // Future: limit access to our own "apps" only
      if(err) {
        var err = new er.IOError("Error registering printer: "+err);
        debug("Error registering printer: "+err);
        return res.status(400).send({err:err});
      }
      res.status(200).send(printer.toObject());
    });
  };
};
