var debug = require('debug')('cloud:slicerConfigEncoder');
var fs = require('fs');
var async = require('async');

module.exports = SlicerConfigEncoder;

function SlicerConfigEncoder() {
  if (!(this instanceof SlicerConfigEncoder)) return new SlicerConfigEncoder();
  var alterations;
  var _self = this;
  function loadAlterationFiles(callback) {
    alterations = {};
    var folder = __dirname+'/alterations/';
    fs.readdir(folder,function(err,files) {
      //debug("load alteration files: ",files.join(', '));
      async.each(files,function(fileName,cb) {
        fs.readFile(folder+fileName,{encoding :'utf8'},function(err,contents){
          alterations[fileName] = contents;
          cb(err,contents);
        });
      },function(err) {
        callback(err);
      });
    });
  }

  this.encode = function(config,extruderCount,callback) {
    //debug("encode");
    if(alterations === undefined) {
      loadAlterationFiles(function(err) {
        if(err) throw new Error("Couldn't load alteration files: "+err);
        _self.encode(config,extruderCount,callback);
      });
      return;
    }
    if(extruderCount === undefined) {
      extruderCount = 1;
    } else if(typeof extruderCount === 'function') {
      callback = extruderCount;
      extruderCount = 1;
    }
    //debug("  config: ",config);
    var encoded = {
      layerThickness: config.layer_height*1000,
      initialLayerThickness: (config.bottom_thickness > 0.0)? config.bottom_thickness * 1000 : config.layer_height * 1000,
      filamentDiameter: config.filament_diameter * 1000,
      filamentFlow: config.filament_flow,
      extrusionWidth: calculateEdgeWidth() * 1000,
      layer0extrusionWidth: calculateEdgeWidth() * config.layer0_width_factor / 100 * 1000,
      insetCount: calculateLineCount(),
      downSkinCount: (config.solid_bottom)? calculateSolidLayerCount() : 0,
      upSkinCount: (config.solid_top)? calculateSolidLayerCount() : 0,
      infillOverlap: config.fill_overlap,
      initialSpeedupLayers: 4,
      initialLayerSpeed: config.bottom_layer_speed,
      printSpeed: config.print_speed,
      infillSpeed: (config.infill_speed > 0)? config.infill_speed : config.print_speed,
      inset0Speed: (config.inset0_speed > 0)? config.inset0_speed : config.print_speed,
      insetXSpeed: (config.insetx_speed > 0)? config.insetx_speed : config.print_speed,
      moveSpeed: config.travel_speed,
      fanSpeedMin: (config.fan_enabled)? config.fan_speed : 0,
      fanSpeedMax: (config.fan_enabled)? config.fan_speed_max : 0,
      supportAngle: (config.support == 'None')? -1 : config.support_angle,
      supportEverywhere: (config.support == 'Everywhere')? 1 : 0,
      supportLineDistance: (config.support_fill_rate > 0)? 100 * calculateEdgeWidth() * 1000 / config.support_fill_rate : -1,
      supportXYDistance: config.support_xy_distance * 1000,
      supportZDistance: config.support_z_distance * 1000,
      supportExtruder: (config.support_dual_extrusion == 'First extruder')? 0 : ((config.support_dual_extrusion == 'Second extruder' && minimalExtruderCount() > 1)? 1 : -1),
      retractionAmount: (config.retraction_enable)? config.retraction_amount * 1000 : 0,
      retractionSpeed: config.retraction_speed,
      retractionMinimalDistance: config.retraction_min_travel * 1000,
      retractionAmountExtruderSwitch: config.retraction_dual_amount * 1000,
      retractionZHop: config.retraction_hop * 1000,
      minimalExtrusionBeforeRetraction: config.retraction_minimal_extrusion * 1000,
      enableCombing: (config.retraction_combing)? 1 : 0,
      multiVolumeOverlap: config.overlap_dual * 1000,
      objectSink: Math.max(0, config.object_sink * 1000),
      minimalLayerTime: config.cool_min_layer_time,
      minimalFeedrate: config.cool_min_feedrate,
      coolHeadLift: (config.cool_head_lift)? 1 : 0,
      startCode: getAlterationFileContents(config['start.gcode'], extruderCount),
      endCode: getAlterationFileContents(config['end.gcode'], extruderCount),
      preSwitchExtruderCode: getAlterationFileContents(config['preSwitchExtruder.gcode'], extruderCount),
      postSwitchExtruderCode: getAlterationFileContents(config['postSwitchExtruder.gcode'], extruderCount),
      fixHorrible: 0
    };
    encoded.extruderOffset = [{},
                              { X:config.extruder_offset_x1 * 1000,
                                  Y: config.extruder_offset_y1 * 1000},
                              { X:config.extruder_offset_x2 * 1000,
                                  Y: config.extruder_offset_y2 * 1000},
                              { X:config.extruder_offset_x3 * 1000,
                                  Y: config.extruder_offset_y3 * 1000}
                             ];

    var fanFullHeight = config.fan_full_height * 1000;
    encoded.fanFullOnLayerNr = (fanFullHeight - encoded.initialLayerThickness - 1) / encoded.layerThickness + 1;
    if(encoded.fanFullOnLayerNr < 0) encoded.fanFullOnLayerNr = 0;
    if(config.support_type == 'Lines') encoded.supportType = 1;
    if(config.fill_density === 0) {
      encoded.sparseInfillLineDistance = -1;
    } else if(config.fill_density == 100) {
      encoded.sparseInfillLineDistance = encoded.extrusionWidth;
      // Set the up/down skins height to 10000 if we want a 100% filled object.
      //  This gives better results then normal 100% infill as the sparse and up/down skin have some overlap.
      encoded.downSkinCount = 10000;
      encoded.upSkinCount = 10000;
    } else {
      encoded.sparseInfillLineDistance = 100 * calculateEdgeWidth() * 1000 / config.fill_density;
    }
    if(config.platform_adhesion == 'Brim') {
      encoded.skirtDistance = 0;
      encoded.skirtLineCount = config.brim_line_count;
    } else if(config.platform_adhesion == 'Raft') {
      encoded.skirtDistance = 0;
      encoded.skirtLineCount = 0;
      encoded.raftMargin = config.raft_margin * 1000;
      encoded.raftLineSpacing = config.raft_line_spacing * 1000;
      encoded.raftBaseThickness = config.raft_base_thickness * 1000;
      encoded.raftBaseLinewidth = config.raft_base_linewidth * 1000;
      encoded.raftInterfaceThickness = config.raft_interface_thickness * 1000;
      encoded.raftInterfaceLinewidth = config.raft_interface_linewidth * 1000;
      encoded.raftInterfaceLineSpacing = config.raft_interface_linewidth * 1000 * 2.0;
      encoded.raftAirGapLayer0 = config.raft_airgap * 1000;
      encoded.raftBaseSpeed = config.bottom_layer_speed;
      encoded.raftFanSpeed = 100;
      encoded.raftSurfaceThickness = encoded.raftInterfaceThickness;
      encoded.raftSurfaceLinewidth = calculateEdgeWidth() * 1000;
      encoded.raftSurfaceLineSpacing = calculateEdgeWidth() * 1000 * 0.9;
      encoded.raftSurfaceLayers = config.raft_surface_layers;
      encoded.raftSurfaceSpeed = config.bottom_layer_speed;
    } else {
      encoded.skirtDistance = config.skirt_gap * 1000;
      encoded.skirtLineCount = config.skirt_line_count;
      encoded.skirtMinLength = config.skirt_minimal_length * 1000;
    }
    if(config.fix_horrible_union_all_type_a) {
      encoded.fixHorrible |= 0x01;
    }
    if(config.fix_horrible_union_all_type_b) {
      encoded.fixHorrible |= 0x02;
    }
    if(config.fix_horrible_use_open_bits) {
      encoded.fixHorrible |= 0x10;
    }
    if(config.fix_horrible_extensive_stitching) {
      encoded.fixHorrible |= 0x04;
    }
    if(encoded.layerThickness <= 0) {
      encoded.layerThickness = 1000;
    }
    if(config.gcode_flavor == 'UltiGCode') {
      encoded.gcodeFlavor = 1;
    } else if(config.gcode_flavor == 'MakerBot') {
      encoded.gcodeFlavor = 2;
    } else if(config.gcode_flavor == 'BFB') {
      encoded.gcodeFlavor = 3;
    } else if(config.gcode_flavor == 'Mach3') {
      encoded.gcodeFlavor = 4;
    } else if(config.gcode_flavor == 'RepRap (Volumetric)') {
      encoded.gcodeFlavor = 5;
    }
    if(config.spiralize) {
      encoded.spiralizeMode = 1;
    }
    if(config.simple_mode) {
      encoded.simpleMode = 1;
    }
    if(config.wipe_tower && extruderCount > 1) {
      encoded.wipeTowerSize = Math.sqrt(config.wipe_tower_volume * 1000 * 1000 * 1000 / encoded.layerThickness);
    }
    if(config.ooze_shield) {
      encoded.enableOozeShield = 1;
    }

    // convert all numbers to integers
    for(var key in encoded) {
      if(typeof encoded[key] === "number") {
        encoded[key] = Math.floor(encoded[key]);
      }
    }
    callback(null,encoded);

    function calculateEdgeWidth() {
      var wallThickness = config.wall_thickness;
      var nozzleSize = config.nozzle_size;

      if(config.spiralize || config.simple_mode) {
        return wallThickness;
      }
      if(wallThickness < 0.01) return nozzleSize;
      if(wallThickness < nozzleSize) return wallThickness;
      var lineCount = Math.floor(wallThickness / (nozzleSize - 0.0001));
      if(lineCount === 0) return nozzleSize;
      var lineWidth = wallThickness / lineCount;
      var lineWidthAlt = wallThickness / (lineCount + 1);
      if(lineWidth > nozzleSize * 1.5) return lineWidthAlt;
      return lineWidth;
    }
    function calculateLineCount() {
      var wallThickness = config.wall_thickness;
      var nozzleSize = config.nozzle_size;
      if (wallThickness < 0.01) return 0; 
      else if (wallThickness < nozzleSize) return 1;
      else if (config.spiralize || config.simple_mode) return 1;
      
      var lineCount = Math.floor(wallThickness / (nozzleSize - 0.0001));
      if (lineCount < 1) lineCount = 1;
      var lineWidth = wallThickness / lineCount;
      if (lineWidth > nozzleSize * 1.5) return lineCount + 1;
      return lineCount;
    }
    function calculateSolidLayerCount() {
      var layerHeight = config.layer_height;
      var solidThickness = config.solid_layer_thickness;
      if(layerHeight === 0.0) return 1;
      return Math.ceil(solidThickness / (layerHeight - 0.0001));
    }
    function minimalExtruderCount() {
      if(config.extruder_amount < 2) return 1;
      if(config.support == 'None') return 1;
      if(config.support_dual_extrusion == 'Second extruder') return 2;
      return 1;
    }
    function getAlterationFileContents(filename, extruderCount) {
      if(extruderCount === undefined) extruderCount = 1;
//      debug("getAlterationFileContents: ",filename);
      var prefix = '';
      var postfix = '';
      var alterationContents = getAlterationFile(filename);
//      debug("  alterationContents: ",alterationContents);
//      debug("  config.gcode_flavor: ",config.gcode_flavor);
      if(config.gcode_flavor == 'UltiGCode'){
        if(filename == 'end.gcode') {
          return 'M25 ;Stop reading from this point on.\n;CURA_PROFILE_STRING:'+getProfileString()+'\n';
        }
        return '';
      }
      if(filename == 'start.gcode') {
        if(extruderCount > 1) {
          alterationContents = getAlterationFile("start"+extruderCount+".gcode");
        }
        // For the start code, hack the temperature and the steps per E value into it. So the temperature is reached before the start code extrusion.
        // We also set our steps per E here, if configured.
        var eSteps = config.steps_per_e;
        if(eSteps > 0) {
          prefix += 'M92 E'+eSteps+'\n';
        }
        var temp = config.print_temperature;
        var bedTemp = 0;
        if(config.has_heated_bed) {
          bedTemp = config.print_bed_temperature;
        }

        if(bedTemp > 0 && alterationContents.indexOf('{print_bed_temperature}') === -1) {
          prefix += 'M190 S'+bedTemp+'\n';
        }
        if(temp > 0 && alterationContents.indexOf('{print_temperature}') === -1) {
          if(extruderCount > 0) {
            var t;
            var n;
            for(n=1;n<extruderCount;n++) {
              t = temp;
              if(n > 0 && config['print_temperature'+(n+1)] > 0) {
                t = config['print_temperature'+(n+1)];
              }
              prefix += 'M104 T'+n+' S'+t+'\n';
            }
            for(n=0;n<extruderCount;n++) {
              t = temp;
              if(n > 0 && config['print_temperature'+(n+1)] > 0) {
                t = config['print_temperature'+(n+1)];
              }
              prefix += 'M109 T'+n+' S'+t+'\n';
            }
            prefix += 'T0\n';
          } else {
            prefix += 'M109 S'+temp+'\n';
          }
        }
        var heatedBedReplacement = (config.has_heated_bed && bedTemp > 0)? "" : ";";
        alterationContents = alterationContents.replace(/{if heatedBed}/gi,heatedBedReplacement);
      } else if(filename == 'end.gcode') {
        if(extruderCount > 1) {
          alterationContents = getAlterationFile("end"+extruderCount+".gcode");
        }
        // Append the profile string to the end of the GCode, so we can load it from the GCode file later.
        // postfix = ';CURA_PROFILE_STRING:%s\n' % (getProfileString())
      }
      //debug("  >alterationContents: ",alterationContents);
      alterationContents = alterationContents.replace(/(.)\{([^\}]*)\}/g, replaceTagMatch);
      //debug("  >>alterationContents: ",alterationContents);
//      alterationContents = alterationContents.rstrip(); //ToDo: rstrip
      alterationContents = prefix+alterationContents+'\n'+postfix;
//      alterationContents = unicode(alterationContents);
//      alterationContents = alterationContents.encode('utf-8') + '\n';
      return alterationContents;
    }
    // Get aleration raw contents
    function getAlterationFile(filename) {
      return alterations[filename];
    }
    function replaceTagMatch(match,pre,tag) {
//      debug("  replaceTagMatch: ",tag);
      var now;
      switch(tag) {
        case 'time':
          //return pre + time.strftime('%H:%M:%S');
          now = new Date();
          return pre + now.getHours()+':'+now.getMinutes()+':'+now.getSeconds();
        case 'date':
          //return pre + time.strftime('%d-%m-%Y');
          now = new Date();
          return pre + now.getDate()+'-'+now.getMonth()+'-'+now.getFullYear();
        case 'day':
          var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
          now = new Date();
          return pre + days[now.getDay()];
        case 'print_time':
          return pre + '#P_TIME#'; // Future: print_time (#P_TIME#)
        case 'filament_amount':
          return pre + '#F_AMNT#'; // Future: filament_amount (#F_AMNT#)
        case 'filament_weight':
          return pre + '#F_WGHT#'; // Future: filament_weight (#F_WGHT#)
        case 'filament_cost':
          return pre + '#F_COST#'; // Future: filament_cost (#F_COST#)
        case 'profile_string':
          return pre + 'CURA_PROFILE_STRING:'+getProfileString();
      }
      var f;
      if(pre == 'F' && tag == 'max_z_speed') {
        f = config.travel_speed * 60;
      }
      if(pre == 'F' && ['print_speed', 'retraction_speed', 'travel_speed', 'bottom_layer_speed', 'cool_min_feedrate'].indexOf(tag) !== -1) {
        f = config[tag] * 60;
      } else if(isConfig(tag)) {
        f = config[tag];
      } else {
        return pre+'?'+tag+'?';
      }
      if((f % 1) === 0) {
        return pre + Math.round(f);
      }
      return pre + f;
    }
    function isConfig(name) {
      for(var key in config) {
        if(key == name) return true;
      }
      return false;
    }

    function getProfileString() {
      return ''; // Future: getProfileString
    }
  };
}
