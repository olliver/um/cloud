var yaml = require('read-yaml').sync;

var slicerConfigEncoder = require('./slicerConfigEncoder')();
var debug = require('debug')('cloud:userConfig');
var SLICER_IGNORES = ['name'];
//var SLICERS = {
//                curaEngine: ['stl','obj'],
//                none: ['gcode']
//              };
var SLICERS = {
                curaEngine: ['stl']
              };

module.exports = UserConfig;

function UserConfig() {
  if (!(this instanceof UserConfig)) return new UserConfig();
  var defaultConfig = yaml('lib/config/defaultConfig.yml');
  //debug("defaultConfig: ",defaultConfig);
  var _self = this;
  this.get = function() {
    return defaultConfig;
  };

  this.getSlicer = function(contentType) {
    for(var slicer in SLICERS) {
      var contentTypes = SLICERS[slicer];
      if(contentTypes.indexOf(contentType) !== -1) {
        return slicer;
      }
    }
  };
  this.getSlicerConfig = function(contentType,overrides,callback) {
    debug("getSlicerConfig: ",contentType,overrides);
    // determine slicer needed for contentType
    var slicer = _self.getSlicer(contentType);
    if(slicer === "none") {
      return callback(null,{});
    } else if(slicer === undefined) {
      return callback(new Error("Unkown contentType"));
    }
    // clone defaults

    if(defaultConfig[slicer] === undefined) {
      return callback(new Error("Slicer "+slicer+" config not found"));
    }
    var slicerConfigDefaults = defaultConfig[slicer].defaults;
    // create basic config from slicer's defaults
    var config = {};
    for(var key in slicerConfigDefaults) {
      config[key] = slicerConfigDefaults[key];
    }
    //debug("  config: ",config);
    // override the config with config from overrides, like machine:ultimaker2
    for(var overrideGroupName in overrides) {
      var overrideSelection = overrides[overrideGroupName];
      //debug("  override with "+overrideGroupName+": "+overrideSelection);
      var overrideGroup = defaultConfig[slicer][overrideGroupName];
      if(overrideGroup === undefined) {
        return callback(new Error(overrideGroupName+" config not found"));
      }
      var overridingConfig = overrideGroup[overrideSelection];
      if(overridingConfig === undefined) {
        return callback(new Error(overrideSelection+" config not found"));
      }

      for(key in overridingConfig) {
        if(SLICER_IGNORES.indexOf(key) !== -1) continue;
        //debug("    ",key,":",config[key],">",overridingConfig[key]);
        config[key] = overridingConfig[key];
      }
    }
    callback(null,config);
  };
  this.getSlicerConfigEncoded = function(contentType,overrides,settings,callback) {
    //debug("getSlicerConfigEncoded: ",contentType,overrides);
    _self.getSlicerConfig(contentType,overrides,function(err,config) {
      if(err) return callback(err,config);
      var slicer = _self.getSlicer(contentType);
      if(slicer === "none") return callback(null,{});
      slicerConfigEncoder.encode(config,function(err,encoded) {
        // override encoded config with specified (overruling) settings
        for(var key in settings) {
          encoded[key] = settings[key];
        }
        callback(err,encoded);
      });

    });
  };
  this.getStructure = function(source) {
    if(source === undefined) source = defaultConfig;
    var structure = {};
    for(var prop in source) {
      if(typeof source[prop] !== 'object') continue;
      structure[prop] = _self.getStructure(source[prop]);
    }
    return structure;
  };
  this.getSlicers = function() {
    var slicersOptions = {};
    for(var slicerName in SLICERS) {
      var slicerOptions = {contentTypes:SLICERS[slicerName]};
      var slicerConfig = defaultConfig[slicerName];
      for(var overrideGroupName in slicerConfig) {
        if(overrideGroupName === 'defaults') continue;
        var overrideGroup = {};
        for(var overrideOptionName in slicerConfig[overrideGroupName]) {
          var overrideOptionConfig = slicerConfig[overrideGroupName][overrideOptionName];
          overrideGroup[overrideOptionName] = overrideOptionConfig.name || overrideOptionName;
        }
        slicerOptions[overrideGroupName] = overrideGroup;
      }
      slicersOptions[slicerName] = slicerOptions;
    }
    return slicersOptions;
  };
}
