module.exports.PORT = 5000;
module.exports.REDIS_HOST = 'localhost';
module.exports.REDIS_PORT = 6379;
module.exports.TCP_HOST = 'localhost';
module.exports.TCP_PORT = 6000;
module.exports.SOCKET_IO_OPTS = {pingInterval:10000,
                                 pingTimeout:20000};
module.exports.SOCKET_IO_STREAM_OPTS = { highWaterMark:16 * 1024 };
module.exports.LOOK_PORT = 7000;
module.exports.STREAM_TIMEOUT = 10000;