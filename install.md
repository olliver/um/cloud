# Installation
``` bash
git clone git@github.com:Doodle3D/Cloud.git
cd Cloud
npm install
``` 
## Redis
Database used for caching and inter-process communication (pub/sub). <br>
http://redis.io<br>
For Debian Wheezy package is available, for Debian squeeze view: <br>
https://chris-lamb.co.uk/posts/official-redis-packages-debian-squeeze

## MongoDB
Database used for persistent storage, like user accounts. <br>
https://www.mongodb.org/

### Installation
- Debian: http://docs.mongodb.org/manual/tutorial/install-mongodb-on-debian/
- CentOS: http://docs.mongodb.org/manual/tutorial/install-mongodb-on-red-hat-centos-or-fedora-linux/

## HAProxy 1.5
We use HAProxy as proxy, load balancer (with sticky sessions) and SSL terminator. 
We require HAProxy 1.5 for SSL termination. This means HAProxy decrypts the SSL and passes on the unencrypted request to the internal services. 
We use HAProxy as load balancer because it enables sticky sessions based on url parameters. Socket.io requires requires sticky sessions. <br>
http://www.haproxy.org/

### Installation
Debian: http://haproxy.debian.net/

### Configuration
Configuration: config/haproxy.cfg<br>
More configuration information:
- http://blog.davidmisshula.com/blog/2013/02/04/configure-haproxy-to-scale-multiple-nodes-with-stickiness-and-ssl/
- http://blog.carbonfive.com/2013/05/02/using-haproxy-with-socket-io-and-ssl/

## PM2
We use PM2 as process manager. It also keeps our processes alive. <br>
(We do not use it's load balancing functionality) <br>
https://github.com/Unitech/pm2

### Installation
``` bash
sudo npm install pm2 -g
```

### Start
Start 4 instances:
``` bash
DEBUG=cloud*,morgan* DEBUG_COLORS=1 pm2 start -f -n cloud1 index.js
PORT=5001 TCP_PORT=6001 DEBUG=cloud*,morgan* DEBUG_COLORS=1 pm2 start -f -n cloud2 index.js
PORT=5002 TCP_PORT=6002 DEBUG=cloud*,morgan* DEBUG_COLORS=1 pm2 start -f -n cloud3 index.js
PORT=5003 TCP_PORT=6003 DEBUG=cloud*,morgan* DEBUG_COLORS=1 pm2 start -f -n cloud4 index.js
``` 

More info: 

- `DEBUG=cloud*,morgan*`: makes sure the right parts are logged. ([more info](https://github.com/visionmedia/debug))
- `DEBUG_COLORS=1`: enable colors in pm2 logs and log files
- `PORT=...`: change the http and websocket server ports
- `TCP_PORT=...`: change the TCP server port (this is used internally to share files)

### Logs
You can view all the ongoing logging using `pm2 logs`.<br>
Using `pm2 desc <id>` you can find the log files. Use `less -r` to view the colors. 

