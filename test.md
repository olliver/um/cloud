# Testing
We use [Mocha](http://mochajs.org/) as test framework and [Should](https://github.com/shouldjs/should.js) as assertion library. 

## Running tests
In 1st terminal window:
``` javascript
export DEBUG=cloud*
node index.js
```
In 2nd terminal window:
``` javascript
export DEBUG=cloud*
PORT=5001 TCP_PORT=6001 node index.js
```
In 3rd terminal window:
``` javascript
export DEBUG=cloud*
npm test
```